﻿using UnityEngine;
using UnityEditor;
using System.IO;
using LitJson;
using System;

public class DataBaseEditor : EditorWindow
{
    private TextAsset myJsonTextAsset;
    private static DataBase dataBase;
    private int planetViewIndex = 0;
    private int starViewIndex = 0;
    private int cardViewIndex = 0;
    private bool unfoldStarLevels = false;
    private bool unfoldPlanetLevels = false;

    private enum EditableObjects { Planets, Stars, Cards }
    private static EditableObjects currentEditableObjects = EditableObjects.Cards;

    [MenuItem("Window/Data Base Editor %#e")]
    static void Init ()
    {
        EditorWindow myWindow = GetWindow(typeof(DataBaseEditor));
        myWindow.minSize = new Vector2(600, 450);
    }

    void OnEnable ()
    {
        if (EditorPrefs.HasKey("ObjectPath"))
        {
            //Debug.Log("Found Data Base");
            InitDataBase(EditorPrefs.GetString("ObjectPath"));
        }
    }

    private void HighlightAsset ()
    {
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = myJsonTextAsset;
    }

    private void InitDataBase (string path)
    {
        myJsonTextAsset = AssetDatabase.LoadAssetAtPath(path, typeof(TextAsset)) as TextAsset;
        //dataBase = new DataBase(JsonMapper.ToObject(myJsonTextAsset.text));
        //dataBase = CreateInstance<DataBase>();
        dataBase = AssetDatabase.LoadAssetAtPath<DataBase>("Assets/ScriptableInstances/StaticDataBase/SODataBase.asset");
        dataBase.InitDataBase(JsonMapper.ToObject(myJsonTextAsset.text));
        //dataBase.InitDataBase();

        planetViewIndex = 0;
        starViewIndex = 0;
        cardViewIndex = 0;
        unfoldStarLevels = false;
        unfoldPlanetLevels = false;
        
        currentEditableObjects = EditableObjects.Planets;
    }

    private void DestroyDataBase ()
    {
        myJsonTextAsset = null;
        dataBase = null;
        EditorPrefs.DeleteKey("ObjectPath");
    }

    private void OnGUI ()
    {
        #region GeneralOptions
        GUILayout.BeginHorizontal();
        GUILayout.Label("Data Base Editor", EditorStyles.boldLabel);

        if (myJsonTextAsset == null)
        {
            if (GUILayout.Button("Open existing data base", GUILayout.ExpandWidth(true)))
            {
                OpenDataBase();
            }
            else if (GUILayout.Button("Create new data base", GUILayout.ExpandWidth(true)))
            {
                CreateNewDataBase();
            }
        }
        else
        {
            if (GUILayout.Button("Select data base asset"))
            {
                HighlightAsset();
            }
            else if (GUILayout.Button("Close data base asset"))
            {
                DestroyDataBase();
            }
            else if (GUILayout.Button("Open other data base"))
            {
                DestroyDataBase();
                OpenDataBase();
            }
        }
        GUILayout.EndHorizontal();
        #endregion GeneralOptions        

        if (myJsonTextAsset != null && dataBase != null)
        {
            dataBase.Version = EditorGUILayout.TextField("Current version", dataBase.Version);
            currentEditableObjects = (EditableObjects) EditorGUILayout.EnumPopup("Object selected:", currentEditableObjects);
            EditorStyles.textField.wordWrap = true;
            switch (currentEditableObjects)
            {
                case EditableObjects.Planets:
                    ShowPlanets();
                    break;
                case EditableObjects.Stars:
                    ShowStars();
                    break;
                case EditableObjects.Cards:
                    ShowCards();
                    break;
            }

            EditorGUILayout.Space();
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Save changes", GUILayout.ExpandWidth(false)))
            {
                SaveDataBase();
            }
            else if (GUILayout.Button("Revert changes", GUILayout.ExpandWidth(false)))
            {
                RevertChanges();
            }
            else if (GUILayout.Button("Create/Refresh scriptables", GUILayout.ExpandWidth(false)))
            {
                CreateScriptables();
            }
            EditorGUILayout.EndHorizontal();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(myJsonTextAsset);
            }
        }
        
    }

    private void CreateScriptables ()
    {
        float progressConstant = 1f / (dataBase.Planets.Length + dataBase.Stars.Length + dataBase.Cards.Length);
        float progress = 0f;
        EditorUtility.DisplayProgressBar("Creating/Refreshing", "Program is creating new items and refreshing existent", progress * progressConstant);

        foreach (PlanetData planetData in dataBase.Planets)
        {
            Planet newPlanet = Planet.CreateInstance(planetData);
            newPlanet.name = planetData.Name;

            string relativePath = "ScriptableInstances/Items/Planets";
            string path = string.Format("Assets/{0}", relativePath);

            Planet planetToRefresh = AssetDatabase.LoadAssetAtPath<Planet>(string.Format("{0}/{1}/{1}.asset", path, newPlanet.name)) ?? null;
            
            if (planetToRefresh == null)
            {
                string planetFolder = string.Format("{0}/{1}", path, newPlanet.name);
                AssetDatabase.CreateFolder(path, newPlanet.name);

                string skinFolder = string.Format("{0}/Skins", planetFolder);
                AssetDatabase.CreateFolder(planetFolder, "Skins");
                
                PlanetSkin planetSkin = CreateInstance<PlanetSkin>();
                newPlanet.CurrentSkin = planetSkin;
                
                AssetDatabase.CreateAsset(planetSkin, string.Format("{0}/{1}BasicSkin.asset", skinFolder, newPlanet.name));
                AssetDatabase.CreateAsset(newPlanet, string.Format("{0}/{1}.asset", planetFolder, newPlanet.name));

            }
            else
            {
                EditorUtility.CopySerialized(newPlanet, planetToRefresh);
                EditorUtility.SetDirty(planetToRefresh);
                AssetDatabase.SaveAssets();
            }
            EditorUtility.DisplayProgressBar("Creating/Refreshing", "Program is creating new items and refreshing existent", ++progress * progressConstant);
        }
        foreach (StarData starData in dataBase.Stars)
        {
            Star newStar = Star.CreateInstance(starData);
            newStar.name = starData.Name;

            string relativePath = "ScriptableInstances/Items/Stars";
            string path = string.Format("Assets/{0}", relativePath);
            
            Star starToRefresh = AssetDatabase.LoadAssetAtPath<Star>(string.Format("{0}/{1}/{1}.asset", path, newStar.name)) ?? null;
            
            if (starToRefresh == null)
            {
                string starFolder = string.Format("{0}/{1}", path, newStar.name);
                AssetDatabase.CreateFolder(path, newStar.name);

                string skinFolder = string.Format("{0}/Skins", starFolder);
                AssetDatabase.CreateFolder(starFolder, "Skins");

                Skin starSkin = CreateInstance<Skin>();
                newStar.CurrentSkin = starSkin;

                AssetDatabase.CreateAsset(starSkin, string.Format("{0}/{1}BasicSkin.asset", skinFolder, newStar.name));
                AssetDatabase.CreateAsset(newStar, string.Format("{0}/{1}.asset", starFolder, newStar.name));
            }
            else
            {
                EditorUtility.CopySerialized(newStar, starToRefresh);
                EditorUtility.SetDirty(starToRefresh);
                AssetDatabase.SaveAssets();
            }
            EditorUtility.DisplayProgressBar("Creating/Refreshing", "Program is creating new items and refreshing existent", ++progress * progressConstant);
        }
        foreach (CardData cardData in dataBase.Cards)
        {
            Card newCard = Card.CreateInstance(cardData);
            newCard.name = cardData.Name;

            string relativePath = "ScriptableInstances/Items/Cards";
            string path = string.Format("Assets/{0}", relativePath);

            Card cardToRefresh = AssetDatabase.LoadAssetAtPath<Card>(string.Format("{0}/{1}/{1}.asset", path, newCard.name)) ?? null;

            if (cardToRefresh == null)
            {
                AssetDatabase.CreateFolder(path, newCard.name);
                path += string.Format("/{0}", newCard.name);
                AssetDatabase.CreateAsset(newCard, string.Format("{0}/{1}.asset", path, newCard.name));
                AssetDatabase.CreateAsset(CreateInstance(typeof(CardSkill)), string.Format("{0}/{1}Skill.asset", path, newCard.name));
            }
            else
            {
                EditorUtility.CopySerialized(newCard, cardToRefresh);
                EditorUtility.SetDirty(cardToRefresh);
                AssetDatabase.SaveAssets();
            }
            EditorUtility.DisplayProgressBar("Creating/Refreshing", "Program is creating new items and refreshing existent", ++progress * progressConstant);
        }

        EditorUtility.ClearProgressBar();
    }

    private void ShowPlanets ()
    {
        #region PlanetSelection
        string[] planetsInDataBase = new string[dataBase.Planets.Length];
        for (int i = 0; i < planetsInDataBase.Length; i++)
        {
            planetsInDataBase[i] = dataBase.Planets[i].Name;
        }
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("<", GUILayout.ExpandWidth(false)))
        {
            planetViewIndex = ( dataBase.Planets.Length + ( planetViewIndex - 1 ) ) % dataBase.Planets.Length;
        }
        GUILayout.Space(5);
        //viewIndex = Mathf.Clamp(EditorGUILayout.IntField(viewIndex + 1, GUILayout.MaxWidth(40)), 1, dataBase.planets.Length) - 1;
        planetViewIndex = EditorGUILayout.Popup(planetViewIndex, planetsInDataBase, GUILayout.MaxWidth(100));
        GUILayout.Space(5);
        if (GUILayout.Button(">", GUILayout.ExpandWidth(false)))
        {
            planetViewIndex = ( planetViewIndex + 1 ) % dataBase.Planets.Length;
        }
        else if (GUILayout.Button("Add"))
        {
            AddPlanet(PlanetData.CreateInstance());
        }
        else if (GUILayout.Button("Delete"))
        {
            DeletePlanet();
        }
        else if (GUILayout.Button("Copy"))
        {
            AddPlanet(dataBase.Planets[planetViewIndex]);
        }
        GUILayout.EndHorizontal();
        #endregion PlanetSelection

        EditorGUILayout.Space();

        #region PlanetCustomization

        PlanetData currentPlanet = dataBase.Planets[planetViewIndex];
        EditorGUIUtility.labelWidth = 75;

        string name = currentPlanet.Name;
        //string ID = currentStar.ID;
        string description = currentPlanet.Description;
        SingleAstronomicalType type = (SingleAstronomicalType) currentPlanet.Type;
        SingleRarityLevel rarity = currentPlanet.Rarity;
        PlanetSkill skill = currentPlanet.Skill;
        byte startCost = ( skill != null ) ? currentPlanet.OverridenSkillCost : (byte) 0;
        PlanetStats[] growthPerLevel = currentPlanet.GrowthPerLevel;
        byte startLife = currentPlanet.Stats.Life;
        sbyte startSize = currentPlanet.Stats.Size;
        byte startAmmo = currentPlanet.Stats.AmmunitionLimit;
        double startCD = currentPlanet.Stats.Cooldown;
        Asteroid[] asteroids = currentPlanet.AsteroidsPerLevel;
        //asteroids[0] = currentPlanet.GetAsteroid(0);
        byte firstAsteroidLife = asteroids[0].Life;
        sbyte firstAsteroidSize = asteroids[0].Size;
        byte firstAsteroidSpeed = asteroids[0].Speed;
        byte firstAsteroidDamage = asteroids[0].Damage;

        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();
        GUILayout.Label(string.Format("Planet ({0})", currentPlanet.ID), EditorStyles.boldLabel);
        name = EditorGUILayout.TextField("Name", currentPlanet.Name, GUILayout.ExpandWidth(true), GUILayout.Width(150));
        //currentPlanet.Type = (Type) EditorGUILayout.EnumPopup("Type", currentPlanet.Type, GUILayout.ExpandWidth(false));
        type = (SingleAstronomicalType) EditorGUILayout.ObjectField("Type", currentPlanet.Type, typeof(SingleAstronomicalType), false);
        //currentPlanet.Rarity = (Rarity) EditorGUILayout.EnumPopup("Rarity", currentPlanet.Rarity, GUILayout.ExpandWidth(false), GUILayout.Width(150));
        rarity = (SingleRarityLevel) EditorGUILayout.ObjectField("Rarity", currentPlanet.Rarity, typeof(SingleRarityLevel), false);
        GUILayout.EndVertical();
        GUILayout.BeginVertical();
        GUILayout.Label("GenericSkill", EditorStyles.boldLabel);
        skill = (PlanetSkill) EditorGUILayout.ObjectField("Skill", skill, typeof(PlanetSkill), false);
        if (skill != null)
            startCost = (byte) EditorGUILayout.IntField("Skill Cost", startCost);
        GUILayout.EndVertical();
        GUILayout.BeginVertical();
        GUILayout.Label("Description", EditorStyles.boldLabel);
        description = EditorGUILayout.TextArea(currentPlanet.Description, GUILayout.Height(50), GUILayout.Width(275), GUILayout.ExpandWidth(false));
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();

        int gridLayoutWidth = 60;
        GUILayout.BeginHorizontal();
        GUILayout.Label("Stats", EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
        GUILayout.Label("Life", EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
        GUILayout.Label("Size", EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
        GUILayout.Label("Max\nAmmo", EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
        GUILayout.Label("Cool-\ndown", EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
        GUILayout.Space(15);
        GUILayout.Label("Asteroid\nLife", EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
        GUILayout.Label("Asteroid\nSize", EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
        GUILayout.Label("Asteroid\nSpeed", EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
        GUILayout.Label("Asteroid\nDamage", EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label(name, EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
        startLife = (byte) EditorGUILayout.IntField(startLife, GUILayout.Width(gridLayoutWidth));
        startSize = (sbyte) EditorGUILayout.IntField(startSize, GUILayout.Width(gridLayoutWidth));
        startAmmo = (byte) EditorGUILayout.IntField(startAmmo, GUILayout.Width(gridLayoutWidth));
        startCD = EditorGUILayout.DoubleField(startCD, GUILayout.Width(gridLayoutWidth));
        GUILayout.Space(15);
        firstAsteroidLife = (byte) EditorGUILayout.IntField(firstAsteroidLife, GUILayout.Width(gridLayoutWidth));
        firstAsteroidSize = (sbyte) EditorGUILayout.IntField(firstAsteroidSize, GUILayout.Width(gridLayoutWidth));
        firstAsteroidSpeed = (byte) EditorGUILayout.IntField(firstAsteroidSpeed, GUILayout.Width(gridLayoutWidth));
        firstAsteroidDamage = (byte) EditorGUILayout.IntField(firstAsteroidDamage, GUILayout.Width(gridLayoutWidth));
        GUILayout.EndHorizontal();

        PlanetStats startStats = new PlanetStats(startLife, startSize, startAmmo, startCD);
        asteroids[0].Copy(new Asteroid(firstAsteroidLife, firstAsteroidSize, firstAsteroidSpeed, firstAsteroidDamage));

        unfoldPlanetLevels = EditorGUILayout.Foldout(unfoldPlanetLevels, "Levels growth");
        if (unfoldPlanetLevels)
        {

            for (byte i = 0; i < growthPerLevel.Length; i++)
            {
                PlanetStats planet = currentPlanet.GetLevelGrowth(i);

                GUILayout.BeginHorizontal();
                byte life = planet.Life;
                sbyte size = planet.Size;
                byte ammo = planet.AmmunitionLimit;
                double CD = planet.Cooldown;
                //Asteroid asteroid = currentPlanet.GetAsteroid((byte) ( i + 1 ));
                Asteroid asteroid = asteroids[i + 1];
                byte asteroidLife = asteroid.Life;
                sbyte asteroidSize = asteroid.Size;
                byte asteroidSpeed = asteroid.Speed;
                byte asteroidDamage = asteroid.Damage;

                GUILayout.Label(( i + 1 ).ToString(), EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
                life = (byte) EditorGUILayout.IntField(life, GUILayout.Width(gridLayoutWidth));
                size = (sbyte) EditorGUILayout.IntField(size, GUILayout.Width(gridLayoutWidth));
                ammo = (byte) EditorGUILayout.IntField(ammo, GUILayout.Width(gridLayoutWidth));
                CD = EditorGUILayout.DoubleField(CD, GUILayout.Width(gridLayoutWidth));
                GUILayout.Space(15);
                asteroidLife = (byte) EditorGUILayout.IntField(asteroidLife, GUILayout.Width(gridLayoutWidth));
                asteroidSize = (sbyte) EditorGUILayout.IntField(asteroidSize, GUILayout.Width(gridLayoutWidth));
                asteroidSpeed = (byte) EditorGUILayout.IntField(asteroidSpeed, GUILayout.Width(gridLayoutWidth));
                asteroidDamage = (byte) EditorGUILayout.IntField(asteroidDamage, GUILayout.Width(gridLayoutWidth));
                GUILayout.EndHorizontal();

                planet = new PlanetStats(life, size, ammo, CD);
                growthPerLevel[i].Copy(planet);
                asteroid = new Asteroid(asteroidLife, asteroidSize, asteroidSpeed, asteroidDamage);
                asteroids[i + 1].Copy(asteroid);
            }
        }
        PlanetData newData = PlanetData.CreateInstance(name, currentPlanet.ID, description, startCost, type, rarity, skill, startStats, growthPerLevel, asteroids);
        dataBase.Planets[planetViewIndex].Copy(newData);
        #endregion
    }

    private void ShowStars ()
    {
        #region StarSelection
        string[] starsInDataBase = new string[dataBase.Stars.Length];
        for (int i = 0; i < starsInDataBase.Length; i++)
        {
            starsInDataBase[i] = dataBase.Stars[i].Name;
        }
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("<", GUILayout.ExpandWidth(false)))
        {
            starViewIndex = ( dataBase.Stars.Length + ( starViewIndex - 1 ) ) % dataBase.Stars.Length;
        }
        GUILayout.Space(5);
        //viewIndex = Mathf.Clamp(EditorGUILayout.IntField(viewIndex + 1, GUILayout.MaxWidth(40)), 1, dataBase.planets.Length) - 1;
        starViewIndex = EditorGUILayout.Popup(starViewIndex, starsInDataBase, GUILayout.MaxWidth(100));
        GUILayout.Space(5);
        if (GUILayout.Button(">", GUILayout.ExpandWidth(false)))
        {
            starViewIndex = ( starViewIndex + 1 ) % dataBase.Stars.Length;
        }
        else if (GUILayout.Button("Add"))
        {
            //AddNewStar();
            AddStar(StarData.CreateInstance());
        }
        else if (GUILayout.Button("Delete"))
        {
            DeleteStar();
        }
        else if (GUILayout.Button("Copy"))
        {
            //CopyStar();
            AddStar(dataBase.Stars[starViewIndex]);
        }
        GUILayout.EndHorizontal();
        #endregion StarSelection

        EditorGUILayout.Space();

        #region StarCustomization
        StarData currentStar = dataBase.Stars[starViewIndex];
        EditorGUIUtility.labelWidth = 75;

        string name = currentStar.Name;
        //string ID = currentStar.ID;
        string description = currentStar.Description;
        SingleAstronomicalType type = (SingleAstronomicalType) currentStar.Type;
        SingleRarityLevel rarity = currentStar.Rarity;
        StarSkill skill = currentStar.Skill;
        StarStats[] growthPerLevel = currentStar.GrowthPerLevel;
        byte startLife = currentStar.Stats.Life;
        sbyte startSize = currentStar.Stats.Size;
        double startCD = currentStar.Stats.Cooldown;

        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();
        GUILayout.Label(string.Format("Star ({0})", currentStar.ID), EditorStyles.boldLabel);
        name = EditorGUILayout.TextField("Name", name, GUILayout.ExpandWidth(true), GUILayout.Width(150));
        type = (SingleAstronomicalType) EditorGUILayout.ObjectField("Type", type, typeof(SingleAstronomicalType), false);
        rarity = (SingleRarityLevel) EditorGUILayout.ObjectField("Rarity", rarity, typeof(SingleRarityLevel), false);
        skill = (StarSkill) EditorGUILayout.ObjectField("Skill", skill, typeof(StarSkill), false);
        GUILayout.EndVertical();
        GUILayout.BeginVertical();
        GUILayout.Label("Description", EditorStyles.boldLabel);
        description = EditorGUILayout.TextArea(description, GUILayout.Height(75), GUILayout.Width(400), GUILayout.ExpandWidth(false));
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();

        int gridLayoutWidth = 80;
        GUILayout.BeginHorizontal();
        GUILayout.Label("Stats", EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
        GUILayout.Label("Life", EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
        GUILayout.Label("Size", EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
        GUILayout.Space(15);
        GUILayout.Label("GenericSkill\nCooldown", EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label(name, EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
        startLife = (byte) EditorGUILayout.IntField(startLife, GUILayout.Width(gridLayoutWidth));
        startSize = (sbyte) EditorGUILayout.IntField(startSize, GUILayout.Width(gridLayoutWidth));
        GUILayout.Space(15);
        startCD = EditorGUILayout.DoubleField(startCD, GUILayout.Width(gridLayoutWidth));
        GUILayout.EndHorizontal();

        StarStats startStats = new StarStats(startLife, startSize, startCD);

        unfoldStarLevels = EditorGUILayout.Foldout(unfoldStarLevels, "Levels growth");
        if (unfoldStarLevels)
        {
            for (int i = 0; i < growthPerLevel.Length; i++)
            {
                StarStats star = growthPerLevel[i];
                byte life = star.Life;
                sbyte size = star.Size;
                double CD = star.Cooldown;

                GUILayout.BeginHorizontal();
                GUILayout.Label(( i + 1 ).ToString(), EditorStyles.boldLabel, GUILayout.Width(gridLayoutWidth));
                life = (byte) EditorGUILayout.IntField(life, GUILayout.Width(gridLayoutWidth));
                size = (sbyte) EditorGUILayout.IntField(size, GUILayout.Width(gridLayoutWidth));
                GUILayout.Space(15);
                CD = EditorGUILayout.DoubleField(CD, GUILayout.Width(gridLayoutWidth));
                GUILayout.EndHorizontal();

                star = new StarStats(life, size, CD);
                growthPerLevel[i].Copy(star);
            }
        }
        StarData newData = StarData.CreateInstance(name, currentStar.ID, description, type, rarity, skill, startStats, growthPerLevel);
        dataBase.Stars[starViewIndex].Copy(newData);
        #endregion
    }

    private void ShowCards ()
    {
        #region CardSelection
        string[] cardsInDataBase = new string[dataBase.Cards.Length];
        for (int i = 0; i < cardsInDataBase.Length; i++)
        {
            cardsInDataBase[i] = dataBase.Cards[i].Name;
        }
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("<", GUILayout.ExpandWidth(false)))
        {
            cardViewIndex = ( dataBase.Cards.Length + ( cardViewIndex - 1 ) ) % dataBase.Cards.Length;
        }
        GUILayout.Space(5);
        //viewIndex = Mathf.Clamp(EditorGUILayout.IntField(viewIndex + 1, GUILayout.MaxWidth(40)), 1, dataBase.planets.Length) - 1;
        cardViewIndex = EditorGUILayout.Popup(cardViewIndex, cardsInDataBase, GUILayout.MaxWidth(100));
        GUILayout.Space(5);
        if (GUILayout.Button(">", GUILayout.ExpandWidth(false)))
        {
            cardViewIndex = ( cardViewIndex + 1 ) % dataBase.Cards.Length;
        }
        else if (GUILayout.Button("Add"))
        {
            //AddNewCard();
            AddCard(CardData.CreateInstance());
        }
        else if (GUILayout.Button("Delete"))
        {
            DeleteCard();
        }
        else if (GUILayout.Button("Copy"))
        {
            //CopyCard();
            AddCard(dataBase.Cards[cardViewIndex]);
        }
        GUILayout.EndHorizontal();
        #endregion CardSelection
        EditorGUILayout.Space();
        #region CardCustomization
        CardData currentCard = dataBase.Cards[cardViewIndex];
        EditorGUIUtility.labelWidth = 75;
        
        string name = currentCard.Name;
        //string ID = currentCard.ID;
        string description = currentCard.Description;
        SingleCardType type = (SingleCardType) currentCard.Type;
        SingleRarityLevel rarity = currentCard.Rarity;
        CardSkill skill = currentCard.Skill;

        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();

        GUILayout.Label(string.Format("Card ({0})", currentCard.ID), EditorStyles.boldLabel);
        name = EditorGUILayout.TextField("Name", name, GUILayout.ExpandWidth(true), GUILayout.Width(150));
        type = (SingleCardType) EditorGUILayout.ObjectField("Type", type, typeof(SingleCardType), false);
        rarity = (SingleRarityLevel) EditorGUILayout.ObjectField("Rarity", rarity, typeof(SingleRarityLevel), false);
        skill = (CardSkill) EditorGUILayout.ObjectField("Skill", skill, typeof(CardSkill), false);
        GUILayout.EndVertical();
        GUILayout.BeginVertical();
        GUILayout.Label("Description", EditorStyles.boldLabel);
        description = EditorGUILayout.TextArea(currentCard.Description, GUILayout.Height(75), GUILayout.Width(300), GUILayout.ExpandWidth(false));
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();

        CardData newData = CardData.CreateInstance(name, currentCard.ID, description, type, rarity, skill);
        dataBase.Cards[cardViewIndex].Copy(newData);
        #endregion
    }

    private void OpenDataBase ()
    {
        //Debug.Log("Open TextAsset file");
        string absPath = EditorUtility.OpenFilePanel("Select Data Base", "", "");
        if (absPath.StartsWith(Application.dataPath))
        {
            string relPath = absPath.Substring(Application.dataPath.Length - "Assets".Length);
            InitDataBase(relPath);

            if (myJsonTextAsset)
            {
                EditorPrefs.SetString("ObjectPath", relPath);
            }
        }
    }
    
    private void CreateNewDataBase ()
    {
        dataBase = CreateInstance<DataBase>();
        dataBase.InitDataBase();

        string path = EditorUtility.SaveFilePanel("New data base", "", "newDataBaseV" + dataBase.Version + ".json", "json");

        if (!path.Equals("") && path != null)
        {
            JsonData databasejson = JsonMapper.ToJson(dataBase);
            //Debug.Log(databasejson);
            File.WriteAllText(path, databasejson.ToString());

            string relPath = path.Substring(Application.dataPath.Length - "Assets".Length);

            AssetDatabase.ImportAsset(relPath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            myJsonTextAsset = AssetDatabase.LoadAssetAtPath(relPath, typeof(TextAsset)) as TextAsset;
            Debug.Log(myJsonTextAsset.text);

            EditorPrefs.SetString("ObjectPath", relPath);
            
            Repaint();
        }
    }

    private void SaveDataBase ()
    {
        string path = "";
        if (EditorUtility.DisplayDialog("Overwrite data base", "Do you want to overwrite this data base?", "Yes", "No, save in new file"))
        {
            path = Application.dataPath + EditorPrefs.GetString("ObjectPath").Substring(6);
        }
        else
        {
            path = EditorUtility.SaveFilePanel( "Save data base", "", EditorPrefs.GetString("ObjectPath").Substring(7), "json");
        }

        if (!path.Equals(""))
        {
            JsonData databasejson = JsonMapper.ToJson(dataBase);
            //Debug.Log(databasejson);
            File.WriteAllText(path, databasejson.ToString());

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }

    private void RevertChanges ()
    {
        OnEnable();
    }
    
    private void AddPlanet (PlanetData newPlanetToAdd = null)
    {
        PlanetData planetToAdd;

        if (newPlanetToAdd == null)
        {
            planetToAdd = PlanetData.CreateInstance(name: string.Format("Default{0}", dataBase.Planets.Length), ID: string.Format("2{0}", dataBase.Planets.Length.ToString("X4")));
        }
        else
        {
            planetToAdd = PlanetData.CreateInstance(newPlanetToAdd.Name + planetViewIndex, string.Format("2{0}", dataBase.Planets.Length.ToString("X4")), newPlanetToAdd.Description, newPlanetToAdd.OverridenSkillCost, (SingleAstronomicalType) newPlanetToAdd.Type, newPlanetToAdd.Rarity, newPlanetToAdd.Skill, newPlanetToAdd.Stats, newPlanetToAdd.GrowthPerLevel, newPlanetToAdd.AsteroidsPerLevel);
        }
        ArrayUtility.Add(ref dataBase.Planets, planetToAdd);
        planetViewIndex = dataBase.Planets.Length - 1;
    }

    private void AddStar (StarData newStarToAdd = null)
    {
        StarData starToAdd;

        if (newStarToAdd == null)
        {
            starToAdd = StarData.CreateInstance(ID: string.Format("1{0}", dataBase.Stars.Length.ToString("X4")));
        }
        else
        {
            starToAdd = StarData.CreateInstance(newStarToAdd.Name + starViewIndex, string.Format("1{0}", dataBase.Stars.Length.ToString("X4")), newStarToAdd.Description, (SingleAstronomicalType) newStarToAdd.Type, newStarToAdd.Rarity, newStarToAdd.Skill, newStarToAdd.Stats, newStarToAdd.GrowthPerLevel);
        }
        ArrayUtility.Add(ref dataBase.Stars, starToAdd);
        starViewIndex = dataBase.Stars.Length - 1;
    }

    private void AddCard (CardData newCardToAdd = null)
    {
        CardData cardToAdd;

        if (newCardToAdd == null)
        {
            cardToAdd = CardData.CreateInstance(ID: string.Format("0{0}", dataBase.Cards.Length.ToString("X4")));
        }
        else
        {
            cardToAdd = CardData.CreateInstance(newCardToAdd.Name + cardViewIndex, string.Format("0{0}", dataBase.Cards.Length.ToString("X4")), newCardToAdd.Description, (SingleCardType) newCardToAdd.Type, newCardToAdd.Rarity, newCardToAdd.Skill);
        }

        ArrayUtility.Add(ref dataBase.Cards, cardToAdd);
        cardViewIndex = dataBase.Cards.Length - 1;
    }

    private void DeletePlanet ()
    {
        DeletePlanet(planetViewIndex);
    }

    private void DeleteStar ()
    {
        DeleteStar(starViewIndex);
    }

    private void DeleteCard ()
    {
        DeleteCard(cardViewIndex);
    }

    private void DeletePlanet (int position)
    {
        if (dataBase.Planets.Length <= 1)
            return;

        ArrayUtility.RemoveAt(ref dataBase.Planets, position);
        if (planetViewIndex > 0)
            planetViewIndex--;
    }

    private void DeleteStar (int position)
    {
        if (dataBase.Stars.Length <= 1)
            return;

        ArrayUtility.RemoveAt(ref dataBase.Stars, position);
        if (starViewIndex > 0)
            starViewIndex--;
    }

    private void DeleteCard (int position)
    {
        if (dataBase.Cards.Length <= 1)
            return;

        ArrayUtility.RemoveAt(ref dataBase.Cards, position);
        if (cardViewIndex > 0)
            cardViewIndex--;
    }
}
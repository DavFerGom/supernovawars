﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AstronomicalTypes))]
public class AstronomicalTypesEditor : Editor
{
    private bool addingType = false;
    private string newTypeName = "Default";
    private AstronomicalTypes currentTarget;

    void OnEnable ()
    {
        currentTarget = (AstronomicalTypes) target;

        bool needToSave = currentTarget.Types.Count > currentTarget.TypeColor.Count || currentTarget.Types.Count > currentTarget.TypeSprite.Count;


        while (currentTarget.Types.Count > currentTarget.TypeColor.Count)
        {
            currentTarget.TypeColor.Add(Color.white);
        }

        while (currentTarget.Types.Count > currentTarget.TypeSprite.Count)
        {
            currentTarget.TypeSprite.Add(new Sprite());
        }

        if (needToSave)
        {
            EditorUtility.SetDirty(currentTarget);
            AssetDatabase.SaveAssets();
        }
    }

    public override void OnInspectorGUI ()
    {
        for (int i = 0; i < currentTarget.Types.Count; i++)
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Remove type", GUILayout.ExpandWidth(false)))
            {
                if (EditorUtility.DisplayDialog("Removing type", "Are you sure you want to delete this type? \nIt could have consecuencies in game logic.", "Yes, remove", "No"))
                {
                    currentTarget.RemoveType(i);
                }
            }
            EditorGUILayout.LabelField(currentTarget.Types[i].name);
            currentTarget.TypeColor[i] = EditorGUILayout.ColorField(currentTarget.TypeColor[i]);
            currentTarget.TypeSprite[i] = (Sprite) EditorGUILayout.ObjectField(currentTarget.TypeSprite[i], typeof(Sprite), false);
            GUILayout.EndHorizontal();
        }

        if (addingType)
        {
            GUILayout.BeginHorizontal();

            newTypeName = GUILayout.TextField(newTypeName);

            if (GUILayout.Button("Add type", GUILayout.ExpandWidth(false)))
            {
                addingType = false;
                currentTarget.CreateNewType(newTypeName);
            }

            if (GUILayout.Button("Cancel", GUILayout.ExpandWidth(false)))
            {
                addingType = false;
            }
            GUILayout.EndHorizontal();
        }
        else
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add type", GUILayout.ExpandWidth(false)))
            {
                addingType = true;
            }
            if (GUILayout.Button("Save changes", GUILayout.ExpandWidth(false)))
            {
                EditorUtility.SetDirty(currentTarget);
                AssetDatabase.SaveAssets();
            }
            GUILayout.EndHorizontal();
        }
    }
}
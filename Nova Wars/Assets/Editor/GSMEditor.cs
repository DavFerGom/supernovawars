﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GameStateMachine))]
public class GMSEditor : Editor
{
    private bool addingType = false;
    private string newTypeName = "Default";
    private GameStateMachine currentTarget;

    void OnEnable ()
    {
        currentTarget = (GameStateMachine) target;
    }

    public override void OnInspectorGUI ()
    {
        for (int i = 0; i < currentTarget.gameStates.Count; i++)
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Remove type", GUILayout.ExpandWidth(false)))
            {
                if (EditorUtility.DisplayDialog("Removing type", "Are you sure you want to delete this type? \nIt could have consecuencies in game logic.", "Yes, remove", "No"))
                {
                    currentTarget.RemoveType(i);
                }
            }
            EditorGUILayout.LabelField(currentTarget.gameStates[i].name);
            GUILayout.EndHorizontal();
        }

        if (addingType)
        {
            GUILayout.BeginHorizontal();

            newTypeName = GUILayout.TextField(newTypeName);

            if (GUILayout.Button("Add type", GUILayout.ExpandWidth(false)))
            {
                addingType = false;
                currentTarget.CreateNewType(newTypeName);
            }

            if (GUILayout.Button("Cancel", GUILayout.ExpandWidth(false)))
            {
                addingType = false;
            }
            GUILayout.EndHorizontal();
        }
        else
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add type", GUILayout.ExpandWidth(false)))
            {
                addingType = true;
            }
            if (GUILayout.Button("Save changes", GUILayout.ExpandWidth(false)))
            {
                EditorUtility.SetDirty(currentTarget);
                AssetDatabase.SaveAssets();
            }
            GUILayout.EndHorizontal();
        }
    }
}
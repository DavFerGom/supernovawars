﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//This class will control all game flow between scenes or game states
public class GameLogic : MonoBehaviour
{
    [SerializeField]
    private GameManager currentGameManager;
    [SerializeField]
    private GameStateMachine gameStateMachine;

    private void Start ()
    {
        currentGameManager.Init();
        gameStateMachine.Initialize(this);
    }

    private void OnApplicationQuit ()
    {
        currentGameManager.DestroyData();
    }

    public void LoadGamePlay ()
    {
        SceneManager.LoadScene(1);
    }
}
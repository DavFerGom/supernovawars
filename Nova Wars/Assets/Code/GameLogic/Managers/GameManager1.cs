﻿using UnityEngine;

//Loading and saving game
public partial class GameManager : ScriptableObject
{
    [SerializeField]
    private GameData currentGameData;
//    private bool _lastSaveHasChanged = false;
    //TODO: Set a routine to save game when something change in GameData

    [ContextMenu("SaveData")]
    private void SaveGameData ()
    {
        LoadSavePlayerGameData.SaveGameData(currentGameData);
    }

    private bool LoadGameData ()
    {
        return LoadSavePlayerGameData.LoadGameData(ref currentGameData);
    }
}
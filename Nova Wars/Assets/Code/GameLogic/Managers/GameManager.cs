﻿using UnityEngine;

//Class initialization
[CreateAssetMenu(menuName = "Tools/Game Manager")]
public partial class GameManager : ScriptableObject
{
    private bool playedBefore = true;
    CardTypes currentCardTypes;

    public void Init ()
    {
        InitStaticDataBase();
        playedBefore = LoadGameData();

        if (!playedBefore)
        {
            SaveGameData();
        }
    }

    public void DestroyData ()
    {
        SaveGameData();
        ResetDataBase();
    }

#if UNITY_EDITOR
    [ContextMenu("Destroy data file")]
    public void DestroyDataFile ()
    {
        LoadSavePlayerGameData.RemoveSavedData();
    }
#endif
}
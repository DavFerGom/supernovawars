﻿using UnityEngine;
using LitJson;

//Static databases
public partial class GameManager : ScriptableObject
{
    [SerializeField]
    private TextAsset myJsonTextAsset;
    [SerializeField]
    private DataBase myDB;
    
    private void InitStaticDataBase ()
    {
        JsonData myJsonData = JsonMapper.ToObject(myJsonTextAsset.text);
        string currentVersion = myJsonData["Version"].ToString();
        if (myDB == null ^ !myDB.Version.Equals(currentVersion))
            myDB.InitDataBase(myJsonData);
        else
            Debug.Log("Up to date!");
    }

    private void ResetDataBase ()
    {
        myDB.InitDataBase();
        currentGameData.Clear();
    }
}
﻿using UnityEngine;
using System.Collections.Generic;

//Dynamics databases
public partial class GameManager : ScriptableObject
{
    private void InitDynamicDataBaseFromGameData (GameData loadedData = null)
    {
        if (loadedData == null)
        {
            currentGameData.playerSeenItems.Init();
            return;
        }

        currentGameData.playerCollection = loadedData.playerCollection;
        currentGameData.playerSeenItems = loadedData.playerSeenItems;
    }
}
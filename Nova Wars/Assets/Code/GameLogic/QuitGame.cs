﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class QuitGame : MonoBehaviour
{
    [SerializeField]
    private UnityEvent ResponseToCancel;

	private void Update ()
	{
        if (Input.GetButtonDown("Cancel"))
        {
            ResponseToCancel.Invoke();
        }
	}

    public void QuitTheGame ()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
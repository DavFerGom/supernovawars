﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGameScene : MonoBehaviour
{
    internal static event FloatParamDelegate OnLoadingLapse;

	private void Awake ()
	{
        StartCoroutine(LoadScene());
	}

    private IEnumerator LoadScene ()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(1);
        asyncLoad.allowSceneActivation = false;

        while(asyncLoad.progress < .9f)
        {
            yield return null;
        }

        asyncLoad.allowSceneActivation = true;
    }
}
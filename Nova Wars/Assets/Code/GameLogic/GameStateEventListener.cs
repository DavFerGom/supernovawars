﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameStateEventListener : MonoBehaviour
{
    [SerializeField]
    private GameState stateToReactTo;

    [SerializeField]
    private StateEvents stateEventToReact;
    [SerializeField]
    private UnityEvent Reaction;

    private void Awake ()
	{
		Initialize();
	}

	private void Initialize ()
	{
        switch (stateEventToReact)
        {
            case StateEvents.Enter:
                stateToReactTo.OnEnterState += Reaction.Invoke;
                break;
            case StateEvents.Stay:
                stateToReactTo.OnStayState += Reaction.Invoke;
                break;
            case StateEvents.Leave:
                stateToReactTo.OnExitState += Reaction.Invoke;
                break;
        }
	}

    private void OnDestroy ()
    {
        switch (stateEventToReact)
        {
            case StateEvents.Enter:
                stateToReactTo.OnEnterState -= Reaction.Invoke;
                break;
            case StateEvents.Stay:
                stateToReactTo.OnStayState -= Reaction.Invoke;
                break;
            case StateEvents.Leave:
                stateToReactTo.OnExitState -= Reaction.Invoke;
                break;
        }
    }
}
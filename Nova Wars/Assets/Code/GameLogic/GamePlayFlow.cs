﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayFlow : MonoBehaviour
{
    [SerializeField]
    private Health playerStarHealth;
    [SerializeField]
    private Health[] playerPlanetsHealth;

    public static event StandardDelegate OnGameReset;
    public static event StandardDelegate OnGameStarted;
    public static event StandardDelegate OnGameEnded;
    public static event StandardDelegate OnPlayerWon;
    public static event StandardDelegate OnPlayerLost;
    public static event StandardDelegate OnDraw;
    public static event IntParamDelegate OnCountDownToStart;

    [SerializeField]
    private Health enemyStarHealth;
    [SerializeField]
    private Health[] enemyPlanetsHealth;

    [SerializeField]
    private Timer timer;
    [SerializeField]
    private int timeToStart = 3;
    [SerializeField]
    private bool playing = false;
    private bool extraTime = false;

    private void Awake ()
    {
        timer.OnFinishingTime += Timer_OnFinishingTimer;
        playerStarHealth.OnDeath += PlayerStarHealth_OnDeath;
        enemyStarHealth.OnDeath += EnemyStarHealth_OnDeath;
    }

    private void Start ()
	{
        StartCoroutine(Gameplay());
	}

    private void EnemyStarHealth_OnDeath ()
    {
        playing = false;
        Debug.Log("You win");
        if (OnPlayerWon != null)
            OnPlayerWon();

        OnGameEnded();
    }

    private void PlayerStarHealth_OnDeath ()
    {
        playing = false;
        Debug.Log("You lose");
        if (OnPlayerLost != null)
            OnPlayerLost();

        OnGameEnded();
    }

    private byte GetAmountOfPlanetsAlive (Health[] playerPlanetsHealth)
    {
        byte planetsAlive = 0;
        for (int i = 0; i < playerPlanetsHealth.Length; i++)
        {
            if (playerPlanetsHealth[i].currentLife > 0)
            {
                ++planetsAlive;
            }
        }

        return planetsAlive;
    }

    private void Timer_OnFinishingTimer ()
    {
        if (!playing)
            return;

        byte enemiesPlanetAlive = GetAmountOfPlanetsAlive(enemyPlanetsHealth);
        byte playerPlanetAlive = GetAmountOfPlanetsAlive(playerPlanetsHealth);
        
        if (playerPlanetAlive == enemiesPlanetAlive)
        {
            if (extraTime)
            {
                playing = false;
                extraTime = false;
                if (OnDraw != null)
                    OnDraw();
            }
            else
            {
                extraTime = true;
                timer.StarTimer(60);
                Debug.Log("Extra time!");
                return;
            }
        }
        else if (playerPlanetAlive > enemiesPlanetAlive)
        {
            //¿Sería mejor idea hacer morir a la estrella mediante Health?
            EnemyStarHealth_OnDeath();
        }
        else
        {
            PlayerStarHealth_OnDeath();
        }

        OnGameEnded();
    }

    private void Initialize ()
    {
        playing = true;
        timer.Initialize(this);
        timer.StarTimer();
    }

    private IEnumerator Gameplay ()
    {
        float timeToStart = this.timeToStart;

        while (timeToStart > 0)
        {
            if (OnCountDownToStart != null)
                OnCountDownToStart(Mathf.RoundToInt(timeToStart));
            timeToStart -= Time.deltaTime;
            yield return null;
        }

        if (OnGameStarted != null)
            OnGameStarted();

        Initialize();
        
        yield return new WaitUntil(() => !playing);
        timer.StopTimer();
    }

    public void ResetGame ()
    {
        //UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        //TODO: Create event to reset and restore every single piece of data
        if (OnGameReset != null)
        {
            OnGameReset();
            StartCoroutine(Gameplay());
        }
    }
}
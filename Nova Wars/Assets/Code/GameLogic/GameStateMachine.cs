﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameStateMachine : ScriptableObject
{
    [SerializeField]
    private float SplashScreenDuration = 2.5f;
    public List<GameState> gameStates = new List<GameState>();

    private GameState currentState;

    private MonoBehaviour initializer;

    public void Initialize (MonoBehaviour initializer)
    {
        currentState = null;
        this.initializer = initializer;

        foreach (GameState gs in gameStates)
        {
            gs.Initialize(initializer);
        }

        ChangeState(gameStates[0]);
        this.initializer.StartCoroutine(SplashScreenTransitionToMenu());
    }

    public void ChangeState (GameState nextState)
    {
        if (currentState == nextState && nextState != null)
            return;

        if (currentState != null)
        {
            currentState.LeaveState();
        }

        currentState = nextState;
        currentState.ToState();
    }

    private IEnumerator SplashScreenTransitionToMenu ()
    {
        yield return new WaitForSeconds(SplashScreenDuration);
        ChangeState(gameStates[1]);
    }

#if UNITY_EDITOR
    [ContextMenu("Create new type")]
    public void CreateNewType (string newAssetName = "Default")
    {
        GameState newGameState = CreateInstance<GameState>();
        newGameState.name = newAssetName;
        gameStates.Add(newGameState);
        AssetDatabase.AddObjectToAsset(newGameState, this);

        AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(newGameState));

        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
    }

    [ContextMenu("Remove type")]
    public void RemoveType (int typeToRemove = -1)
    {
        if (typeToRemove < 0)
        {
            typeToRemove = gameStates.Count - 1;
        }

        GameState newGameState = gameStates[typeToRemove];
        gameStates.RemoveAt(typeToRemove);

        DestroyImmediate(newGameState, true);
        AssetDatabase.SaveAssets();
    }
#endif
}
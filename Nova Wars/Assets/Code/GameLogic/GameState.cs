﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu]
public class GameState : ScriptableObject
{
	private MonoBehaviour initializer;

	public void Initialize (MonoBehaviour initializer)
	{
		this.initializer = initializer;
    }

    public bool InState { private set; get; }

    public event StandardDelegate OnEnterState;
    public event StandardDelegate OnStayState;
    public event StandardDelegate OnExitState;

    [SerializeField]
    private UnityEvent EnterState;
    [SerializeField]
    private UnityEvent StayState;
    [SerializeField]
    private UnityEvent ExitState;

    public void ToState ()
    {
        //Debug.Log(string.Format("Entering to {0} state...", name));
        if (OnEnterState != null)
            OnEnterState();
        InState = true;

        if (initializer != null && OnStayState != null)
        {
            WhileInState();
        }
    }
    
    public void LeaveState ()
    {
        //Debug.Log(string.Format("Leaving {0} state...", name));
        if (OnExitState != null)
            OnExitState();
        InState = false;
    }

    private IEnumerator WhileInState ()
    {
        while (InState)
        {
            //Debug.Log(string.Format("{0} state...", name));
            OnStayState();
            yield return null;
        }
    }
}

public enum StateEvents { Enter, Stay, Leave }
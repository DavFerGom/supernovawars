﻿using UnityEngine;

[CreateAssetMenu(menuName = "Brain")]
public class Brain : ScriptableObject
{
    [Range(0.02f, 10f)]
    [SerializeField]
    private float timeBetweenEvaluations;
    [Range(0.02f, 10f)]
    [SerializeField]
    private float minTimeBetweenAttacks;
    public float MinTimeBetweenAttacks
    {
        get
        {
            return minTimeBetweenAttacks;
        }
    }
    [Range(0.02f, 10f)]
    [SerializeField]
    private float maxTimeBetweenAttacks;
    public float MaxTimeBetweenAttacks
    {
        get
        {
            return maxTimeBetweenAttacks;
        }
    }
    [Range(0f, 1f)]
    [SerializeField]
    private float precision;
    public float Precision
    {
        get
        {
            return precision;
        }
    }
    [Range(0f, 1f)]
    [SerializeField]
    private float aggressiveness;
    [SerializeField]
    private EmptyStar ownedStar;
    [SerializeField]
    private EmptyPlanet[] ownedPlanets = new EmptyPlanet[3];
    [SerializeField]
    private EmptyAsteroid[] ownedAsteroids = new EmptyAsteroid[3];
    [SerializeField]
    private EmptyCard[] ownedCards = new EmptyCard[4];
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AI;

[CreateAssetMenu(menuName = "AI/Shoot")]
public class ShooterAction : Action
{
    public override void Act (StateController controller)
    {
        Shoot(controller);
    }

    private void Shoot (StateController controller)
    {
        if (controller.CheckIfCountDownElapsed())
        {
            LaunchAsteroids[] launchers = controller.GetComponentsInChildren<LaunchAsteroids>();
            int randomChildren = Random.Range(0, launchers.Length);
            //Vector2 direction = Vector2.down + new Vector2((1f - controller.CurrentBrain.Precision), 0f);
            float precision = controller.CurrentBrain.Precision + Random.Range(0f, 1f - controller.CurrentBrain.Precision);
            Vector2 direction = Vector2.Lerp(new Vector2(1f, -2f).normalized, Vector2.down, precision);
            direction = new Vector2(direction.x * Mathf.Sign(Random.Range(-1f + controller.CurrentBrain.Precision, 1f - controller.CurrentBrain.Precision)), direction.y * 1f);
            launchers[randomChildren].LaunchNewAsteroid(direction.normalized);

//            Debug.Log(string.Format("Sending asteroid to {0}", controller.Target.position));
            controller.ResetTimeSinceLastAction();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AI;

[CreateAssetMenu(menuName = "AI/Shoot")]
public class CardAction : Action
{
    public override void Act (StateController controller)
    {
        Shoot(controller);
    }

    private void Shoot (StateController controller)
    {
        if (controller.CheckIfCountDownElapsed())
        {

        }
    }
}
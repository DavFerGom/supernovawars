﻿using UnityEngine;
using AI;

public class StateController : MonoBehaviour
{
    [SerializeField]
    private State currentState;
    public State CurrentState
    {
        get
        {
            return currentState;
        }
    }
    [SerializeField]
    private Brain currentBrain;
    public Brain CurrentBrain
    {
        get
        {
            return currentBrain;
        }
    }
    [SerializeField]
    private Transform target;
    public Transform Target
    {
        get
        {
            return target;
        }
    }
    private bool isPlaying = false;
    public bool IsPlaying
    {
        get
        {
            return isPlaying;
        }
        set
        {
            if (isPlaying == value)
            {
                return;
            }

            isPlaying = value;
            if (isPlaying)
            {
                StartState();
            }
            else
            {
                StopState();
            }
        }
    }

    private float timeToNextAction = 0;

    public void ResetTimeSinceLastAction ()
    {
        timeToNextAction = Random.Range(currentBrain.MinTimeBetweenAttacks, currentBrain.MaxTimeBetweenAttacks);
    }

    private void Awake ()
    {
        GamePlayFlow.OnGameStarted += GamePlayFlow_OnGameStarted;
        GamePlayFlow.OnGameEnded += GamePlayFlow_OnGameEnded;
    }

    private void OnDestroy ()
    {
        GamePlayFlow.OnGameStarted -= GamePlayFlow_OnGameStarted;
        GamePlayFlow.OnGameEnded -= GamePlayFlow_OnGameEnded;
    }

    private void GamePlayFlow_OnGameEnded ()
    {
        StopState();
    }

    private void GamePlayFlow_OnGameStarted ()
    {
        StartState();
    }

    private void StopState ()
    {
        StopAllCoroutines();
    }

    private void StartState ()
    {
        StartCoroutine(currentState.UpdateState(this));
    }

    public void TransitionToState (State nextState)
    {
        if (nextState != null && nextState != currentState)
        {
            StopState();
            currentState = nextState;
            StartState();
        }
    }

    public bool CheckIfCountDownElapsed ()
    {
        timeToNextAction -= Time.deltaTime;
        return ( timeToNextAction < 0f );
    }

    private void OnExitState ()
    {
        timeToNextAction = 0;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    [CreateAssetMenu(menuName = "AI/State")]
    public class State : ScriptableObject
    {
        public Action[] actions;
        public Transition[] transitions;

        private void DoActions (StateController controller)
        {
            for (int i = 0; i < actions.Length; i++)
            {
                actions[i].Act(controller);
            }
        }
        
        private void CheckTransitions (StateController controller)
        {
            for (int i = 0; i < transitions.Length; i++)
            {
                controller.TransitionToState(transitions[i].NextState(transitions[i].decision.Decide(controller)));
            }
        }
        
        public IEnumerator UpdateState (StateController controller)
        {
            while (true)
            {
                DoActions(controller);
                CheckTransitions(controller);
                yield return null;
            }
        }
    }
}
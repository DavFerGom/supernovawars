﻿using System;
using UnityEngine;

namespace AI
{
    [Serializable]
    public class Transition
    {
        public Decision decision;
        [SerializeField]
        private State trueState;
        [SerializeField]
        private State falseState;
        public State NextState( bool decisionResult)
        {
            return decisionResult ? trueState : falseState;
        }
    }
}
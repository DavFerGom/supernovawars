﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class TypesList<T> : ScriptableObject where T : ScriptableObject
{
    public List<T> Types = new List<T>();
    public List<Color> TypeColor = new List<Color>();
    public List<Sprite> TypeSprite = new List<Sprite>();

#if UNITY_EDITOR
    [ContextMenu("Create new type")]
    public void CreateNewType (string newAssetName = "Default")
    {
        T newType = CreateInstance<T>();
        newType.name = newAssetName;
        Types.Add(newType);
        TypeColor.Add(Color.white);
        TypeSprite.Add(new Sprite());
        AssetDatabase.AddObjectToAsset(newType, this);
        
        AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(newType));
    }

    [ContextMenu("Remove type")]
    public void RemoveType (int typeToRemove = -1)
    {
        if (typeToRemove < 0)
        {
            typeToRemove = Types.Count - 1;
        }

        T newType = Types[typeToRemove];
        Types.RemoveAt(typeToRemove);
        TypeColor.RemoveAt(typeToRemove);
        TypeSprite.RemoveAt(typeToRemove);

        DestroyImmediate(newType, true);
        AssetDatabase.SaveAssets();
    }

    public void Create (string newAssetName)
    {
        CreateNewType(newAssetName);
    }
#endif

    public byte GetIndex(string typeName)
    {
        return ( byte) Types.FindIndex(x => x.name == typeName);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Types List/Rarity Levels")]
public class RarityLevels : TypesList<SingleRarityLevel>
{
}
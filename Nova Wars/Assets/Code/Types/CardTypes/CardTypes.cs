﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Types List/Card Types")]
public class CardTypes : TypesList<SingleCardType>
{
}
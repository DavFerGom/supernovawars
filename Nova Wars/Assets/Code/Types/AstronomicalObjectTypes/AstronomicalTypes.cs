﻿using UnityEngine;

[CreateAssetMenu(menuName = "Types List/Astronomical Types")]
public class AstronomicalTypes : TypesList<SingleAstronomicalType>
{
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Integer")]
public class Integer : ScriptableObject
{
    [SerializeField]
    private int _number = 0;
    public int number
    {
        get { return _number; }
        set { _number = value; if (AmountChanged != null) AmountChanged(value); }
    }
    public event IntParamDelegate AmountChanged;

    public void Add(int amount)
    {
        number += amount;
    }
}
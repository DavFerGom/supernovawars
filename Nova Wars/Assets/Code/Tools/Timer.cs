﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Timer")]
public class Timer : ScriptableObject
{
	private MonoBehaviour initializer;
    public float CurrentTime { private set; get; }

	public void Initialize (MonoBehaviour initializer)
	{
		this.initializer = initializer;
	}
    
    private bool playing = false;

    public event FloatParamDelegate OnBeginTime;
    public event FloatParamDelegate OnTimeCounting;
    public event StandardDelegate OnFinishingTime;

    //TODO: use extra time.
    public event FloatParamDelegate OnBeginExtraTime;
    public event FloatParamDelegate OnExtraTimeCounting;
    public event StandardDelegate OnFinishingExtraTime;

    public void StarTimer (float time = 180f)
    {
        initializer.StartCoroutine(TimeCounterRounded(time));
    }

    public void StopTimer ()
    {
        playing = false;
        CurrentTime = 0;
    }

    private IEnumerator TimeCounterRounded (float timeToPlay)
    {
        playing = true;
        OnBeginTime(timeToPlay);

        CurrentTime = timeToPlay;

        while (playing && --CurrentTime >= 0f)
        {
            yield return new WaitForSeconds(1f);
            OnTimeCounting(CurrentTime);
        }

        playing = false;
        OnFinishingTime();
    }

    /*IEnumerator TimeCounter ()
    {
        OnBeginTimer();

        float time = timeToPlay;

        while (timeToPlay > 0f)
        {
            yield return null;
            time -= Time.deltaTime;
            OnTimeCounting(time);
        }

        OnFinishingTimer();
    }*/
}
﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using LitJson;
using System;
using System.Security.Cryptography;

public static class LoadSavePlayerGameData
{
    private static readonly string pass = "GyUeLa";
    private static string CompletedPath = Application.persistentDataPath + "/GameData.nwgd";

    public static void SaveGameData (GameData dataToSave)
    {
        //TODO: Sobreescribir partida anterior
        JsonData databasejson = JsonMapper.ToJson(dataToSave);

        Rijndael miRijndael = Rijndael.Create();
        byte[] encrypted = null;
        byte[] returnValue = null;

        try
        {
            miRijndael.Key = new PasswordDeriveBytes(pass, null).GetBytes(32);
            miRijndael.GenerateIV();

            byte[] toEncrypt = System.Text.Encoding.UTF8.GetBytes(databasejson.ToString());
            encrypted = ( miRijndael.CreateEncryptor() ).TransformFinalBlock(toEncrypt, 0, toEncrypt.Length);

            returnValue = new byte[miRijndael.IV.Length + encrypted.Length];
            miRijndael.IV.CopyTo(returnValue, 0);
            encrypted.CopyTo(returnValue, miRijndael.IV.Length);
        }
        catch { }
        finally { miRijndael.Clear(); }

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(CompletedPath);
        bf.Serialize(file, returnValue);
        file.Close();
    }

    public static bool LoadGameData (ref GameData dataToStoreLoading)
    {
        if (!File.Exists(CompletedPath))
            return false;

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(CompletedPath, FileMode.Open);
        byte[] bytDesEncriptar = (byte[]) bf.Deserialize(file);

        Rijndael miRijndael = Rijndael.Create();
        byte[] tempArray = new byte[miRijndael.IV.Length];
        byte[] encrypted = new byte[bytDesEncriptar.Length - miRijndael.IV.Length];
        string returnValue = string.Empty;

        try
        {
            miRijndael.Key = new PasswordDeriveBytes(pass, null).GetBytes(32);

            Array.Copy(bytDesEncriptar, tempArray, tempArray.Length);
            Array.Copy(bytDesEncriptar, tempArray.Length, encrypted, 0, encrypted.Length);
            miRijndael.IV = tempArray;

            returnValue = System.Text.Encoding.UTF8.GetString(( miRijndael.CreateDecryptor
() ).TransformFinalBlock(encrypted, 0, encrypted.Length));
        }
        catch { }
        finally { miRijndael.Clear(); }

        GameData loaded = JsonMapper.ToObject<GameData>(returnValue);
        file.Close();

        if (loaded != null)
        {
            dataToStoreLoading.Init(loaded);
            return true;
        }
        else
            return false;
    }

    public static void RemoveSavedData ()
    {
        File.Delete(CompletedPath);
    }
}
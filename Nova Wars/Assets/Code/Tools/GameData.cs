﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(menuName = "Data bases/Game data")]
public class GameData : ScriptableObject
{
    public Collection playerCollection;
    public SeenObjects playerSeenItems;
    public Lineup[] lineups = new Lineup[3];
    public Materials playerMaterials;
    public Integer coins;
    //materiales, monedas, skins y niveles, último nivel jugado y último nivel desbloqueado.

    public void Init (GameData dataToLoad)
    {
        playerCollection.Init(dataToLoad.playerCollection);
        playerSeenItems.Init(dataToLoad.playerSeenItems);
        for (int i = 0; i < lineups.Length; i++)
        {
            lineups[i].Init(dataToLoad.lineups[i]);
        }
        for (int i = 0; i < playerMaterials.materials.Count; i++)
        {
            playerMaterials.materials[i].materialAmount = dataToLoad.playerMaterials.materials[i].materialAmount;
        }

        coins.number = dataToLoad.coins.number;
    }

    public void Clear ()
    {
        playerCollection.Clear();
        playerSeenItems.Clear();
        for (int i = 0; i < lineups.Length; i++)
        {
            lineups[i].Clear();
        }
        for (int i = 0; i < playerMaterials.materials.Count; i++)
        {
            playerMaterials.materials[i].materialAmount = 0;
        }
        coins.number = 0;
    }

    private void OnDisable ()
    {
        Clear();
    }
}
﻿using UnityEngine;

public delegate void FloatParamDelegate (float floatParam);
public delegate void IntParamDelegate (int intParam);
public delegate void MonoBehaviourDelegate (MonoBehaviour mono);
public delegate void StarDelegate (Star star);
public delegate void PlanetDelegate (Planet planet);
public delegate void CardDelegate (Card card);
public delegate void AsteroidDelegate (Asteroid asteroid);
public delegate void MonoBehaviourParamsDelegate (params MonoBehaviour[] monos);
public delegate void StandardDelegate ();
﻿using System;
using UnityEngine;
using UnityEngine.Purchasing;

[CreateAssetMenu(menuName = "Hard Market/Manager")]
public class HardMarket : ScriptableObject, IStoreListener
{
    //TODO: show error panel with Debug.LogError messages
    //TODO: check if purchase has been made before?
    private static IStoreController controller;
    private static IExtensionProvider extension;
    private HardSale[] productDatas;
    public HardSale this[string ID]
    {
        get
        {
            for (int i = 0; i < controller.products.all.Length; i++)
            {
                if (string.Equals(productDatas[i].ID, ID))
                    return productDatas[i];
            }

            return null;
        }
    }

    public void Initialize ()
    {
        if (controller != null)
            return;

        var constructor = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        foreach (HardSale item in productDatas)
        {
            constructor.AddProduct(item.ID, item.type);
        }

        //Intenta conectar con el servidor de compras
        UnityPurchasing.Initialize(this, constructor);
    }

    public void OnInitialized (IStoreController _controller, IExtensionProvider _extension)
    {
        controller = _controller;
        extension = _extension;

        //Products are ready
    }

    public void OnInitializeFailed (InitializationFailureReason error)
    {
        //TODO: Store not available. Please, check your connection and try again.
        //Turn off store hard coin products
        Debug.Log("ERROR: Sistema de COMPRAS NO inicializado: "+error);
    }

    public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs args)
    {
        try
        {
            this[args.purchasedProduct.definition.id].Purchase.Invoke();
        }
        catch (Exception)
        {
            Debug.Log(String.Format("ProcessPurchaseError: Producto irreconocible: {0}", args.purchasedProduct.definition.id));
        }
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed (Product product, PurchaseFailureReason failureReason)
    {
        //TODO: Sorry, something was wrong. Please, try again. Be calm, your attempt will not be charged.
        //Turn off store hard coin products
        Debug.Log(String.Format("urchaseFailed: Producto: {0} - Motivo: {1}", product.definition.storeSpecificId, failureReason));
    }

    public void BuyProduct (HardSale product)
    {
        if (!IsInitialized())
        {
            Debug.LogError("Error. Purchasing system is not initialized...");
            //TODO: Give a try to Initialize()?
            return;
        }
        
        Product purchasingProduct = controller.products.WithID(product.ID);

        if (purchasingProduct == null || !purchasingProduct.availableToPurchase)
        {
            Debug.LogError(string.Format("Product {0} does not exists or is not available", product.ID), product);
            return;
        }
        
        controller.InitiatePurchase(product.ID);
    }

    bool IsInitialized ()
    {
        return controller != null && extension != null;
    }

    public string GetPriceStringOfProduct (string productId)
    {
        for (int i = 0; i < controller.products.all.Length; i++)
        {
            if (string.Equals(controller.products.all[i].definition.id, productId))
                return controller.products.all[i].metadata.localizedPriceString;
        }
        
        return "n/a";
    }

}
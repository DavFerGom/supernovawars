﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;

[CreateAssetMenu(menuName = "Hard Market/Product")]
public class HardSale : ScriptableObject
{
    [SerializeField]
    private string _ID;
    public string ID { get { return _ID; } }

    [SerializeField]
    private ProductType _type;
    public ProductType type { get { return _type; } }

    public UnityEvent Purchase;
}
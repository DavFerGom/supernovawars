﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Soft Market/Product")]
public class SoftSale : ScriptableObject
{
    public int cost = 100;

    public virtual void Purchase ()
    {

    }
}
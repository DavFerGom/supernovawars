﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Soft Market/Manager")]
public class SoftMarket : ScriptableObject
{
    [SerializeField]
    private Integer coins;
    
    private bool TryToBuy (int moneyNeeded)
    {
        return moneyNeeded <= coins.number;
    }

    public void BuyWithSoftCoins (SoftSale sale)
    {
        if (!TryToBuy(sale.cost))
        {
            Debug.LogWarning("Show message: Not enough money.");
            return;
        }

        coins.Add(-sale.cost);
        sale.Purchase();
    }
}
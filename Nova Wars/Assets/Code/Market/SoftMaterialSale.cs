﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Soft Market/Material Sale")]
public class SoftMaterialSale : SoftSale
{
    [SerializeField]
    private SingleAstronomicalType material;
    [SerializeField]
    private Materials materials;
    [SerializeField]
    private int amount = 0;

    public override void Purchase ()
    {
        materials[material].materialAmount += amount;
    }
}
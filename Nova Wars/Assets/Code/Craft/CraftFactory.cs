﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CraftFactory")]
public class CraftFactory : ScriptableObject
{
	private MonoBehaviour initializer;
    [SerializeField]
    private Collection collection;
    [SerializeField]
    private AstronomicalTypes materials;
    [SerializeField]
    private Materials playerMaterials;
    [SerializeField]
    private List<RequiredMaterials> materialsPerRarity = new List<RequiredMaterials>();

    public bool TryToCraft(Item item, out RequiredMaterials reqMat)
    {
        reqMat = CalculateRequiredMeterials(item);

        for (int i = 0; i < reqMat.requiredMaterials.Length; i++)
        {
            if (playerMaterials[materials.Types[i]].materialAmount < reqMat[materials.Types[i]].materialAmount)
            {
                return false;
            }
        }
        return true;
    }

    public void CraftItem (Item itemToCraft)
    {
        RequiredMaterials reqMat;
        if (!TryToCraft(itemToCraft, out reqMat))
            return;

        if (itemToCraft.GetType().Equals(typeof(Planet)))
        {
            collection.ownedPlanets.Add((Planet)itemToCraft);
        }
        else if (itemToCraft.GetType().Equals(typeof(Star)))
        {
            collection.ownedStars.Add((Star) itemToCraft);
        }
        else if (itemToCraft.GetType().Equals(typeof(Card)))
        {
            collection.ownedCards.Add((Card) itemToCraft);
        }
        
        for (int i = 0; i < reqMat.requiredMaterials.Length; i++)
        {
            playerMaterials[materials.Types[i]].materialAmount -= reqMat[materials.Types[i]].materialAmount;
        }
    }

    private RequiredMaterials CalculateRequiredMeterials(Item item)
    {
        RequiredMaterials reqMat = materialsPerRarity.Find(x => x.rarityLevel.Equals(item.Data.Rarity));
        
        if (item.GetType().Equals(typeof(Card)))
        {
            //TODO: cards maths
        }
        else
        {
            reqMat[(SingleAstronomicalType)item.Data.Type].materialAmount *= 2;
            if (item.GetType().Equals(typeof(Star)))
            {
                for (int i = 0; i < reqMat.requiredMaterials.Length; i++)
                {
                    reqMat.requiredMaterials[i].materialAmount *= 2;
                }
            }
        }
        
        return reqMat;
    }
}
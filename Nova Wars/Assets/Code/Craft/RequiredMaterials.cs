﻿[System.Serializable]
public class RequiredMaterials
{
    public SingleRarityLevel rarityLevel;
    public PlayerMaterial[] requiredMaterials = new PlayerMaterial[3];
    public PlayerMaterial this[SingleAstronomicalType material]
    {
        get
        {
            for (int i = 0; i < requiredMaterials.Length; i++)
            {
                if (requiredMaterials[i].material.Equals(material))
                    return requiredMaterials[i];
            }

            return null;
        }
    }
}
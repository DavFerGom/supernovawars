﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class PlayerMaterial
{
    public SingleAstronomicalType material;
    [SerializeField]
    private int _materialAmount;
    public int materialAmount
    {
        get { return _materialAmount; }
        set { _materialAmount = value; if (AmountChanged != null) AmountChanged(value); }
    }
    public event IntParamDelegate AmountChanged;

    public PlayerMaterial ()
    {
        material = null;
        materialAmount = 0;
    }

    public PlayerMaterial (SingleAstronomicalType material, int materialAmount = 0)
    {
        this.material = material;
        this.materialAmount = materialAmount;
    }
}
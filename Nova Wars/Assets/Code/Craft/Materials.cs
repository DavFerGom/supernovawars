﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Materials")]
public class Materials : ScriptableObject
{
    [SerializeField]
    public List<PlayerMaterial> materials = new List<PlayerMaterial>();
    public PlayerMaterial this[SingleAstronomicalType material]
    {
        get { return materials.Find(x => x.material.Equals(material)); }
        set
        {
            if (!materials.Exists(x => x.material.Equals(material)))
            {
                materials.Add(new PlayerMaterial(material, 0));
            }
        }
    }
}
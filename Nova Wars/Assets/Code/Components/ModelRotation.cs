﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelRotation : MonoBehaviour
{
    [SerializeField]
    public float rotationSpeed = 3.5f;
    [SerializeField]
    private Vector3 rotationDirection;

    private void Update ()
	{
        transform.Rotate(rotationDirection * rotationSpeed, Space.World);
	}
}
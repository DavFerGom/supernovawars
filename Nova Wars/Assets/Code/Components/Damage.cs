﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{
    public byte damage { private set; get; }
    private Health ownHealth;

    public void Initialize (byte damage)
    {
        this.damage = damage;
        ownHealth = GetComponent<Health>();
    }
    
    private void OnTriggerEnter (Collider other)
    {
        Health health = other.transform.parent.GetComponent<Health>();

        if (health != null)
        {
            health.LoseLife(damage);
        }

        if (other.gameObject.layer.ToString().Contains("Asteroid"))
            return;

        ownHealth.LoseLife(ownHealth.currentLife);
    }
}
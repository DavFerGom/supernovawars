﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class LineupInitializer : MonoBehaviour
{
    public static event StandardDelegate OnInitialitionEnded;
    private static bool firstFinished = false;

    [SerializeField]
    private Lineup lineup;
    
    [SerializeField]
    private EmptyStar sceneStar;
    [SerializeField]
    private EmptyPlanet[] scenePlanets;
    [SerializeField]
    private EmptyAsteroid[] sceneAsteroids;
    [SerializeField]
    private EmptyCard[] sceneCards;

    private void Start ()
	{
		Initialize();
	}

	private void Initialize ()
    {
        sceneStar.Init(lineup.currentStar);

        for (int i = 0; i < scenePlanets.Length; i++)
        {
            scenePlanets[i].Init(lineup.currentPlanets[i]);
            sceneAsteroids[i].Initialize(( (PlanetData) lineup.currentPlanets[i].Data ).GetLevelAsteroid(lineup.currentPlanets[i].Level), lineup.currentPlanets[i]);
        }
        
        for (int i = 0; i < sceneCards.Length; i++)
        {
            sceneCards[i].Init(lineup.currentCards[i]);
        }

        if (firstFinished)
            OnInitialitionEnded();
        else
            firstFinished = true;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DisableInteractionOnGameEnded : MonoBehaviour
{
    [SerializeField]
    private PhysicsRaycaster raycaster;

	private void Awake ()
    {
        GamePlayFlow.OnDraw += DisableInteraction;
        GamePlayFlow.OnPlayerLost += DisableInteraction;
        GamePlayFlow.OnPlayerWon += DisableInteraction;
    }

    private void DisableInteraction ()
    {
        raycaster.enabled = enabled = false;
    }

    private void OnDisable ()
    {
        GamePlayFlow.OnDraw -= DisableInteraction;
        GamePlayFlow.OnPlayerLost -= DisableInteraction;
        GamePlayFlow.OnPlayerWon -= DisableInteraction;
    }
}
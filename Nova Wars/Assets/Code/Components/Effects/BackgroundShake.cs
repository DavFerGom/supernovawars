﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BackgroundShake : MonoBehaviour
{
    [SerializeField]
    private DOTweenAnimation myPlanetsTween;

    [SerializeField]
    private Health[] planetHealths;

    [SerializeField]
    private Health[] starHealths;

    private void Awake ()
	{
        for (int i = 0; i < planetHealths.Length; i++)
        {
            planetHealths[i].OnDeath += RunAnimation;
        }

        for (int i = 0; i < starHealths.Length; i++)
        {
            starHealths[i].OnDeath += RunStarAnimation;
        }
    }

    private void RunStarAnimation ()
    {
        myPlanetsTween.DOPlayById("StarsPunch");
    }

    private void RunAnimation ()
	{
        myPlanetsTween.DOPlayById("PlanetsPunch");
	}
}
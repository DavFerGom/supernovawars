﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveDestruction : MonoBehaviour
{
    private ParticleSystem explotion;

	private void Awake ()
    {
        explotion = GetComponentInChildren<ParticleSystem>();
        transform.GetComponent<Health>().OnDeath += Die;
    }

    private void Die ()
    {
        explotion.Play();
    }

    private void OnDestroy ()
    {
        transform.GetComponent<Health>().OnDeath -= Die;
    }
}
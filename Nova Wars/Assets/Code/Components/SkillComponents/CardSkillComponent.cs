﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSkillComponent : MonoBehaviour
{
    private void Awake ()
    {
        LineupInitializer.OnInitialitionEnded += LineupInitializer_OnInitialitionEnded;
    }

    private void LineupInitializer_OnInitialitionEnded ()
    {
        CardComponent cardComponent = GetComponent<CardComponent>();
        InitSkill(( (CardData) cardComponent.SceneCard.currentCard.Data ).Skill ?? null, cardComponent);
    }

    public CardSkill skillBehaviour { get; private set; }

    public void InitSkill (CardSkill skillBehaviour = null, CardComponent sender = null)
    {
        if (skillBehaviour == null)
        {
            Destroy(this);
            return;
        }

        this.skillBehaviour = skillBehaviour;  //TODO: Create a new instance getting its parent properties.

        skillBehaviour.InitSkill(sender);
    }

    public void UseSkill ()
    {
        skillBehaviour.UseSkill();
    }

    private void OnDestroy ()
    {

        LineupInitializer.OnInitialitionEnded -= LineupInitializer_OnInitialitionEnded;
    }
}
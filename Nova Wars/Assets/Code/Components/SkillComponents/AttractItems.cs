﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttractItems : MonoBehaviour
{
    private LayerMask layersAttractable;
    private float attractionForze;

    public void Initialize (LayerMask layersAttractable, float attractionForze)
    {
        this.layersAttractable = layersAttractable;
        this.attractionForze = attractionForze;
    }

	private void OnTriggerStay (Collider other)
	{
        Rigidbody othersRigid = other.attachedRigidbody;

        if (othersRigid == null)
            return;

        othersRigid.AddForce((other.transform.position - transform.position).normalized * -attractionForze, ForceMode.Acceleration);
	}
}
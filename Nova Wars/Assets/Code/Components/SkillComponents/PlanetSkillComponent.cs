﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetSkillComponent : MonoBehaviour
{
    public PlanetSkill skillBehaviour { get; private set; }
    public AsteroidsPool asteroidsPool { get; private set; }

    private void Awake ()
    {
        LineupInitializer.OnInitialitionEnded += LineupInitializer_OnInitialitionEnded;
    }

    private void LineupInitializer_OnInitialitionEnded ()
    {
        PlanetComponent myPlanet = gameObject.GetComponent<PlanetComponent>();

        asteroidsPool = myPlanet.AsteroidsPool;

        Init(( (PlanetData) myPlanet.ScenePlanet.currentPlanet.Data ).Skill ?? null, asteroidsPool);
    }

    public void Init (PlanetSkill skillBehaviour = null, AsteroidsPool asteroidsPool = null)
    {
        if (skillBehaviour == null || asteroidsPool == null)
        {
            Destroy(this);
            return;
        }

        this.skillBehaviour = skillBehaviour;  //TODO: Create a new instance getting its parent properties.
        this.asteroidsPool = asteroidsPool;

        skillBehaviour.InitSkill(this);
        skillBehaviour.OnSkillUsed += SkillBehaviour_OnSkillUsed;

        asteroidsPool.OnAmountChanged += AsteroidsPool_OnAmountChanged;
    }

    private void AsteroidsPool_OnAmountChanged (float floatParam)
    {
        if (asteroidsPool.asteroidsAvailable > skillBehaviour.Cost)
            skillBehaviour.SetReady();
    }

    private void SkillBehaviour_OnSkillUsed (params MonoBehaviour[] monos)
    {
        asteroidsPool.AddAsteroids(-skillBehaviour.Cost);
    }
    
    public void UseSkill ()
    {
        if (asteroidsPool.asteroidsAvailable >= skillBehaviour.Cost)
        {
            skillBehaviour.UseSkill();
        }
    }

    private void OnDestroy ()
    {
        if (skillBehaviour != null)
            skillBehaviour.OnSkillUsed -= SkillBehaviour_OnSkillUsed;
        if (asteroidsPool != null)
            asteroidsPool.OnAmountChanged -= AsteroidsPool_OnAmountChanged;

        LineupInitializer.OnInitialitionEnded -= LineupInitializer_OnInitialitionEnded;
    }
}
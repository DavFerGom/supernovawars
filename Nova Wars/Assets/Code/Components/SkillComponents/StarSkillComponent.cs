﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSkillComponent : MonoBehaviour
{
    public StarSkill skillBehaviour { get; private set; }

    public void Init (StarSkill skillBehaviour = null)
    {
        if (skillBehaviour == null)
        {
            Destroy(this);
            return;
        }

        this.skillBehaviour = skillBehaviour;  //TODO: Create a new instance getting its parent properties.

        skillBehaviour.InitSkill(this);
    }

    public void UseSkill ()
    {
        skillBehaviour.UseSkill();
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableParentOnBecameInvisible : MonoBehaviour
{
    private void OnBecameInvisible ()
	{
        if(transform.parent.gameObject.activeInHierarchy && gameObject.activeSelf)
            transform.parent.gameObject.SetActive(false);
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetsResurrectionManager : MonoBehaviour
{
    [SerializeField]
    private Timer gameplayTime;
    [SerializeField]
    private PlanetHealth[] planetHealths = new PlanetHealth[3];

	private void Awake ()
	{
        planetHealths[0].OnDeath += () => PlanetsResurrectionManager_OnDeath(0);
        planetHealths[1].OnDeath += () => PlanetsResurrectionManager_OnDeath(1);
        planetHealths[2].OnDeath += () => PlanetsResurrectionManager_OnDeath(2);
    }
    
    private void PlanetsResurrectionManager_OnDeath (int index)
    {
        StartCoroutine(ResurrectPlanet(planetHealths[index]));
    }

    IEnumerator ResurrectPlanet (PlanetHealth planetHealth)
    {
        yield return new WaitForSeconds(5f);
        planetHealth.Initialize(planetHealth.maxLife);
    }

    private void OnDestroy ()
    {
        planetHealths[0].OnDeath -= () => PlanetsResurrectionManager_OnDeath(0);
        planetHealths[1].OnDeath -= () => PlanetsResurrectionManager_OnDeath(1);
        planetHealths[2].OnDeath -= () => PlanetsResurrectionManager_OnDeath(2);
    }
}
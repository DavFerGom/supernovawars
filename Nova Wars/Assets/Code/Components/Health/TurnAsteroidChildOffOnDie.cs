﻿using UnityEngine;

public class TurnAsteroidChildOffOnDie : MonoBehaviour
{
    protected Health astronomicalItemHealth;

    protected virtual void Awake ()
    {
        astronomicalItemHealth = transform.parent.GetComponent<Health>();
        if (astronomicalItemHealth != null) astronomicalItemHealth.OnDeath += Die;
    }

    protected void Die ()
    {
        gameObject.SetActive(false);
    }

    protected virtual void OnDestroy ()
    {
        if (astronomicalItemHealth != null) astronomicalItemHealth.OnDeath -= Die;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetHealth : Health
{
    public event StandardDelegate OnResurrection;

    public override void Initialize (int life)
    {
        base.Initialize(life);
        if (OnResurrection != null && !gameObject.activeSelf)
        {
            OnResurrection();
        }
    }

    private void OnDestroy ()
    {
        OnResurrection = null;
    }
}
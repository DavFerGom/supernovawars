﻿public class TurnPlanetChildOffOnDie : TurnAsteroidChildOffOnDie
{
    protected override void Awake ()
    {
        base.Awake();
        ((PlanetHealth) astronomicalItemHealth).OnResurrection += Resurrect;
    }

    private void Resurrect ()
    {
        gameObject.SetActive(true);
    }
    
    protected override void OnDestroy ()
    {
        base.OnDestroy();
        ( (PlanetHealth) astronomicalItemHealth ).OnResurrection -= Resurrect;
    }
}
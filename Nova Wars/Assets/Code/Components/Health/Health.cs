﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public event StandardDelegate OnDeath;
    public event FloatParamDelegate OnHealthChanged;
    public event IntParamDelegate OnInitialized;

    public int maxLife { private set; get; }
    public float inverseMaxLife { private set; get; }
    public int currentLife { private set; get; }
    public bool inmune { private set; get; }

    public void SetImmunity ()
    {
        inmune = true;
    }

    public void StopImmunity ()
    {
        inmune = false;
    }

    public virtual void Initialize (int life)
    {
        currentLife = life;
        if (OnInitialized != null)
            OnInitialized(life);
        ChangeMaxLife(life);
    }

    public void ChangeMaxLife (int life)
    {
        maxLife = life;
        inverseMaxLife = 1f / maxLife;
        LoseLife(0);
    }

    public void ChangeMaxLifeTemporary (int life, float timeToRestore)
    {
        int currentMaxLife = maxLife;

        ChangeMaxLife(life);

        StartCoroutine(RestoreMaxLife(currentMaxLife, timeToRestore));
    }

    private IEnumerator RestoreMaxLife (int life, float timeToRestore)
    {
        yield return new WaitForSeconds(timeToRestore);
        ChangeMaxLife(life);
    }

    public void LoseLife (int lifeToLose)
    {
        if (inmune && lifeToLose > 0)
            return;

        currentLife = Mathf.Clamp(currentLife - lifeToLose, 0, maxLife);

        if (OnHealthChanged != null)
            OnHealthChanged(currentLife * inverseMaxLife);

        if (currentLife == 0 && OnDeath != null)
        {
            OnDeath();
        }
    }
    
    private void OnDestroy ()
    {
        OnDeath = null;
        OnHealthChanged = null;
        OnInitialized = null;
    }
}
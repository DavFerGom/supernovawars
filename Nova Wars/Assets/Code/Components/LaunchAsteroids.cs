﻿using UnityEngine;

public class LaunchAsteroids : MonoBehaviour
{
    private AsteroidsPool asteroids;
    private Collider touchCollider;
    [SerializeField]
    private EmptyAsteroid asteroidStats;
    [SerializeField]
    private EmptyPlanet planetStats;
    private float distance = .21f;
    private bool shooting = false;

    public void Initialize (AsteroidsPool asteroids)
    {
        this.asteroids = asteroids;
        touchCollider = GetComponent<Collider>();
        distance += 0.4f + ( planetStats.currentStats.Size * .1f );
        firstTouchPosition = Camera.main.WorldToScreenPoint(transform.position);
    }

    Vector2 firstTouchPosition = Vector2.zero;
    Vector2 lastTouchPosition = Vector2.zero;
    Vector2 normalizedDirection = Vector2.zero;
    GameObject phantom { get { return asteroids.GetPhantomAsteroid(); } }
    AsteroidMovement newAsteroid;

    public void SetAvailableAsteroid ()
    {
        newAsteroid = asteroids.GetAsteroid();

        if (newAsteroid != null)
            phantom.SetActive(true);
    }

    public void RefreshDirection ()
    {
        if (newAsteroid == null)
            return;

        lastTouchPosition = Input.GetTouch(0).position;
        normalizedDirection = ( lastTouchPosition - firstTouchPosition ).normalized;
        phantom.transform.position = transform.position + (Vector3) ( normalizedDirection * distance );

    }

    public void LaunchNewAsteroid ()
    {
        if (newAsteroid == null)
            return;

        phantom.SetActive(false);

        newAsteroid.transform.position = phantom.transform.position;

        newAsteroid.Launch(normalizedDirection);

        asteroids.AddAsteroids(-1);
    }

    public void LaunchNewAsteroid (Vector3 direction)
    {
        AsteroidMovement newAsteroid = asteroids.GetAsteroid();

        if (newAsteroid == null)
            return;
        
        newAsteroid.transform.position = transform.position + ( direction.normalized * distance );

        newAsteroid.Launch(direction.normalized * asteroidStats.stats.Speed);

        asteroids.AddAsteroids(-1);
    }

    private void Awake ()
    {
        GetComponentInChildren<Health>().OnDeath += Die;
    }

    private void Die ()
    {
        if (phantom != null) phantom.SetActive(false);
        //GetComponentInChildren<UnityEngine.EventSystems.EventTrigger>().GetComponent<MeshCollider>().enabled = true;
    }

    private void OnDestroy ()
    {
        GetComponentInChildren<Health>().OnDeath -= Die;
    }
}
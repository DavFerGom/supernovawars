﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidMovement : MonoBehaviour
{
    private Rigidbody myRigidbody;
    public float Speed { get; private set; }
    
    private const int SPEEDFACTOR = 20;
    private const int MINSPEED = 75;

    public void Awake ()
    {
        myRigidbody = GetComponent<Rigidbody>();
    }

    public void Launch (Vector3 direction)
	{
        gameObject.SetActive(true);
        myRigidbody.AddForce(direction.normalized * MINSPEED + ( direction * SPEEDFACTOR));
        //StartCoroutine(Move(direction * ( .5f + ( 3.5f * Speed ) )));
    }

    private void OnDisable ()
    {
        myRigidbody.Sleep();
    }

    /*private IEnumerator Move (Vector3 direction)
    {
        while (enabled)
        {
            transform.Translate(direction * Time.deltaTime);
            yield return null;
        }
    }*/
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidsPool : MonoBehaviour
{
    public event StandardDelegate OnEmpty;
    public event FloatParamDelegate OnAmountChanged;
    public event IntParamDelegate OnInitialized;

    private List<AsteroidMovement> asteroidsInstantiated = new List<AsteroidMovement>();
    [SerializeField]
    private GameObject phantomAsteroid;

    [SerializeField]
    private GameObject asteroidTemplate;
    public int asteroidsAvailable { get; private set; }
    private bool unlimitedAsteroids = false;

    public void SetUnlimitedAsteroids ()
    {
        AddAsteroids(ammunitionLimit);
        unlimitedAsteroids = true;
    }

    public void LimiteAsteroids ()
    {
        unlimitedAsteroids = false;
    }
    private float inverseAmmunitionLimit = 0; //TODO: refresh if ammunitionlimit changes in game
    private bool filling = false;

    [SerializeField]
    private EmptyAsteroid asteroidStats;
    public EmptyAsteroid AsteroidsStats { get { return asteroidStats; } }
    [SerializeField]
    private EmptyPlanet planetStats;

    private byte ammunitionLimit
    {
        get
        {
            return planetStats.currentStats.AmmunitionLimit;
        }
    }

    [SerializeField]
    private EmptyAsteroid sceneAsteroid;

    private void Awake ()
    {
        sceneAsteroid.OnInitialized += Initialize;
        GamePlayFlow.OnGameEnded += GamePlayFlow_OnGameEnded;
    }

    private void GamePlayFlow_OnGameEnded ()
    {
        StopAllCoroutines();
    }

    public void Initialize (Asteroid asteroid)
    {
        GameObject asteroidMesh = asteroidTemplate.transform.Find("Model").gameObject;
        asteroidStats.SetSkin(asteroidMesh);
        asteroidMesh.layer = gameObject.layer;
        
        asteroidsAvailable = ammunitionLimit;
        if (OnInitialized != null)
            OnInitialized(ammunitionLimit);
        inverseAmmunitionLimit = 1f / ammunitionLimit;
        filling = false;

        asteroidTemplate.GetComponentInChildren<Health>().Initialize(asteroidStats.stats.Life);
        asteroidTemplate.GetComponentInChildren<Damage>().Initialize(asteroidStats.stats.Damage);
    }

    public void RefreshAllAsteroids ()
    {
        asteroidTemplate.GetComponentInChildren<Health>().Initialize(asteroidStats.stats.Life);
        asteroidTemplate.GetComponentInChildren<Damage>().Initialize(asteroidStats.stats.Damage);
        for (int i = 0; i < asteroidsInstantiated.Count; i++)
        {
            asteroidsInstantiated[i].GetComponentInChildren<Health>().Initialize(asteroidStats.stats.Life);
            asteroidsInstantiated[i].GetComponentInChildren<Damage>().Initialize(asteroidStats.stats.Damage);
        }
    }

    public AsteroidMovement GetAsteroid (bool dontCheckAvailables = false)
    {
        if (asteroidsAvailable <= 0 && !dontCheckAvailables)
            return null;

        AsteroidMovement newAsteroid = null;

        if (!dontCheckAvailables)
        {
            for (int i = 0; i < asteroidsInstantiated.Count; i++)
            {
                if (!asteroidsInstantiated[i].gameObject.activeSelf)
                    newAsteroid = asteroidsInstantiated[i];
            }
        }

        if (newAsteroid == null)
        {
            newAsteroid = Instantiate(asteroidTemplate).GetComponent<AsteroidMovement>();
            newAsteroid.transform.SetParent(transform);
            newAsteroid.gameObject.SetActive(false);

            if (!dontCheckAvailables)
                asteroidsInstantiated.Add(newAsteroid);
        }

        newAsteroid.GetComponentInChildren<Health>(true).Initialize(asteroidStats.stats.Life);
        newAsteroid.GetComponentInChildren<Damage>(true).Initialize(asteroidStats.stats.Damage);

        return newAsteroid;
    }

    private IEnumerator Fill ()
    {
        filling = true;
        while (asteroidsAvailable < ammunitionLimit)
        {
            yield return new WaitForSeconds((float) planetStats.currentStats.Cooldown);
            AddAsteroids(1);
        }
        filling = false;
    }

    public void AddAsteroids (int amountToAdd)
    {
        if (unlimitedAsteroids)
            return;

        asteroidsAvailable = Mathf.Clamp(asteroidsAvailable + amountToAdd, 0, ammunitionLimit);

        if (OnAmountChanged != null)
            OnAmountChanged(asteroidsAvailable * inverseAmmunitionLimit);
        
        if (!filling)
            StartCoroutine(Fill());

        if (asteroidsAvailable > 0)
            return;

        if (OnEmpty != null)
        {
            OnEmpty();
        }
    }

    public GameObject GetPhantomAsteroid ()
    {
        if (phantomAsteroid != null && phantomAsteroid.activeSelf)
            return phantomAsteroid;

        if (phantomAsteroid == null)
        {
            GameObject asteroidModel = asteroidTemplate.transform.Find("Model").gameObject;
            phantomAsteroid = Instantiate(asteroidModel, transform);
            Destroy(phantomAsteroid.GetComponent<DisableParentOnBecameInvisible>());
            Destroy(phantomAsteroid.GetComponent<TurnAsteroidChildOffOnDie>());
            phantomAsteroid.name = "PhantomAsteroid";
            phantomAsteroid.transform.localScale = asteroidModel.transform.lossyScale;
            ChangeMaterialRenderingMode.SetupMaterialWithBlendMode(phantomAsteroid.GetComponent<MeshRenderer>().material, BlendMode.Fade);
            phantomAsteroid.GetComponent<MeshRenderer>().material.color = new Color(1f, 1f, 1f, .35f);
            phantomAsteroid.SetActive(false);
        }

        return phantomAsteroid;
    }

    private void OnDestroy ()
    {
        sceneAsteroid.OnInitialized -= Initialize;
        GamePlayFlow.OnGameEnded -= GamePlayFlow_OnGameEnded;
    }
}
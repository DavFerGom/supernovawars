﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarComponent : MonoBehaviour
{
    [SerializeField]
    private EmptyStar sceneStar;
    private StarSkillComponent skill;

    private void Awake ()
    {
        sceneStar.OnInitialized += Initialize;
    }

    public void Initialize (Star myStar)
    {
        skill = GetComponent<StarSkillComponent>();

        GameObject mesh = transform.Find("Model").gameObject;
        ((StarSkin) myStar.CurrentSkin).SetSkin(mesh);
        
        mesh.layer = mesh.transform.parent.gameObject.layer;

        mesh.transform.GetComponentInParent<ModelRotation>().rotationSpeed =
                ( ( (StarData) myStar.Data ).Skill != null ) ? 2f - ( ( (float) ( (StarData) myStar.Data ).Stats.Cooldown ) / 150f ) : .1f;

        float size = ( (StarData) myStar.Data ).Stats.Size;

        transform.localPosition = new Vector3(0f, ( 15f - size ) * .05f);
        transform.localScale *= 0.8f + ( size * .2f );
        
        skill.Init(( (StarData) myStar.Data ).Skill ?? null);

        GetComponentInChildren<Health>().Initialize(( (StarData) myStar.Data ).Stats.Life);
    }

    private void OnDestroy ()
    {
        sceneStar.OnInitialized -= Initialize;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidComponent : MonoBehaviour
{
    public event StandardDelegate SetChildren;

    private void OnEnable ()
    {
        if (SetChildren != null)
            SetChildren();
    }

    private void Start ()
    {
        GetComponentInChildren<Health>().OnDeath += Die;
    }

    private void Die ()
    {
        Invoke("TurnOff", .75f);
    }

    private void TurnOff ()
    {
        gameObject.SetActive(false);

        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
        }
    }

    private void Awake ()
    {
        GamePlayFlow.OnGameEnded += GamePlayFlow_OnGameEnded;
    }

    private void GamePlayFlow_OnGameEnded ()
    {
        Health healthComponent = GetComponentInChildren<Health>();
        if (healthComponent != null)
            healthComponent.LoseLife(100);
    }

    private void OnDestroy ()
    {
        GamePlayFlow.OnGameEnded -= GamePlayFlow_OnGameEnded;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardComponent : MonoBehaviour
{
    [SerializeField]
    private EmptyCard sceneCard;
    public EmptyCard SceneCard { get { return sceneCard; } }

    public SingleCardType sceneCardType { get { return (SingleCardType) ( (CardData) sceneCard.currentCard.Data ).Type; } }

    private void Awake ()
    {
        sceneCard.OnInitialized += Initialize;
    }

    public void Initialize (Card myCard)
    {
        //Debug.Log(myCard.name);
    }

    private void OnDestroy ()
    {
        sceneCard.OnInitialized -= Initialize;
    }
}
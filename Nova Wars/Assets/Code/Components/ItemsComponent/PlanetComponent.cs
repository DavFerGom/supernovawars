﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetComponent : MonoBehaviour
{
    [SerializeField]
    private EmptyPlanet scenePlanet;
    public EmptyPlanet ScenePlanet { get { return scenePlanet; } }

    private PlanetHealth myHealth;

    private void Awake ()
    {
        scenePlanet.OnInitialized += Initialize;

        myHealth = GetComponentInChildren<PlanetHealth>();
    }

    private LaunchAsteroids launcher;
    [SerializeField]
    private AsteroidsPool asteroidsPool;
    public AsteroidsPool AsteroidsPool { get { return asteroidsPool; } }
    
    public void Initialize (Planet myPlanet)
    {
        launcher = GetComponent<LaunchAsteroids>();
        launcher.Initialize(asteroidsPool);
        
        GameObject mesh = transform.Find("Model").gameObject;
        ((PlanetSkin)myPlanet.CurrentSkin).SetSkin(mesh);
        
        mesh.layer = mesh.transform.parent.gameObject.layer;

        mesh.transform.GetComponentInParent<ModelRotation>().rotationSpeed = ( 4f - (float) ( (PlanetData) myPlanet.Data ).Stats.Cooldown ) * 0.5f;

        float size = ( (PlanetData) myPlanet.Data ).Stats.Size;
        transform.localScale *= 0.8f + ( size * .2f );

        myHealth.Initialize(( (PlanetData) myPlanet.Data ).Stats.Life);
        myHealth.OnResurrection += MyHealth_OnResurrection;
        myHealth.OnDeath += PlanetComponent_OnDeath;
    }

    private void MyHealth_OnResurrection ()
    {
        gameObject.SetActive(true);
    }
    
    private void PlanetComponent_OnDeath ()
    {
        transform.parent.gameObject.GetComponent<MonoBehaviour>().StartCoroutine(DieCo());
    }

    IEnumerator DieCo ()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }

    private void OnDestroy ()
    {
        scenePlanet.OnInitialized -= Initialize;
    }
}
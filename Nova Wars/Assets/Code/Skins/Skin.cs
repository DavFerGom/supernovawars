﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Skins/Card")]
public class Skin : ScriptableObject
{
    [SerializeField]
    public bool available;
    [SerializeField]
    public Sprite icon;
    [SerializeField]
    protected SingleRarityLevel skinRarity;

    public virtual void SetIcon (Image icon)
    {
        icon.sprite = this.icon;
        //TODO: set border color depending on rarity.
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skins/Star")]
public class StarSkin : AstronomicalItemSkin
{

    public override void SetSkin (GameObject model)
    {
        base.SetSkin(model);
        model.GetComponent<MeshRenderer>().material.SetTexture("_EmissionMap", texture);
    }
}
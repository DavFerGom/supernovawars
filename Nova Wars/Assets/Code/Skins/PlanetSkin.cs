﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skins/Planet")]
public class PlanetSkin : AstronomicalItemSkin
{
    [SerializeField]
    protected Texture asteroidTexture;
    [SerializeField]
    protected Mesh asteroidMesh;
    
    public void SetAsteroidSkin (GameObject model)
    {
        model.GetComponent<MeshFilter>().mesh = asteroidMesh;
        model.GetComponent<MeshRenderer>().material.mainTexture = asteroidTexture;
    }
}
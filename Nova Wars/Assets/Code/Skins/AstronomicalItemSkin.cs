﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstronomicalItemSkin : Skin
{
    [SerializeField]
    protected Texture texture;
    [SerializeField]
    protected Mesh mesh;

    public virtual void SetSkin (GameObject model)
    {
        model.GetComponent<MeshFilter>().mesh = mesh;
        model.GetComponent<MeshRenderer>().material.mainTexture = texture;
        model.GetComponent<MeshRenderer>().material.SetTexture("_EmissionMap", texture);
    }
}
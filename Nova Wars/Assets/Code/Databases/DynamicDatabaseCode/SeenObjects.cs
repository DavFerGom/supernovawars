﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Data bases/Seen objects")]
public class SeenObjects : ScriptableObject
{
    /// <summary>
    /// If ID does not exist, player has not seen object, otherwise value will say how many of them player owns.
    /// </summary>
    public List<string> objectsOwned { private set; get; }
    //If you already had have one item, you are eable to see its name

    public bool IsDiscovered(string ID)
    {
        return objectsOwned != null && objectsOwned.Contains(ID);
    }

    public void Discover(string ID)
    {
        if (!IsDiscovered(ID))
        {
            objectsOwned.Add(ID);
            Debug.Log(string.Format("{0} was discovered!", ID));
        }
    }

    public void Init (SeenObjects dataToInit)
    {
        if (objectsOwned == null)
            Init();
        else
            Clear();

        if (dataToInit == null || dataToInit.objectsOwned == null)
            return;

        foreach (string ID in dataToInit.objectsOwned)
        {
                Discover(ID);
        }
    }

    public void Init ()
    {
        objectsOwned = new List<string>();
    }

    public void Clear ()
    {
        if (objectsOwned != null)
            objectsOwned.Clear();
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Data bases/Collection")]
public class Collection : ScriptableObject
{
    [SerializeField]
    private DataBase dataBase;
    public PlanetsRuntimeSet ownedPlanets;
    public StarsRuntimeSet ownedStars;
    public CardsRuntimeSet ownedCards;

    public void Init ()
    {
        for (int i = 0; i < 4; i++)
        {
            ownedCards.Add(CreateInstance<Card>());
        }
        for (int i = 0; i < 4; i++)
        {
            ownedPlanets.Add(CreateInstance<Planet>());
        }
        ownedStars.Add(CreateInstance<Star>());
    }

    public void Init (Collection dataToInit)
    {
        foreach (Planet planet in dataToInit.ownedPlanets.Items)
        {
            planet.Init((PlanetData) dataBase.GetObjectData(dataBase.GetPlanetID(planet.name)));
            ownedPlanets.Items.Add(planet);
        }
        foreach (Star star in dataToInit.ownedStars.Items)
        {
            star.Init((StarData) dataBase.GetObjectData(dataBase.GetStarID(star.name)));
            ownedStars.Items.Add(star);
        }
        foreach (Card card in dataToInit.ownedCards.Items)
        {
            card.Init((CardData) dataBase.GetObjectData(dataBase.GetCardID(card.name)));
            ownedCards.Items.Add(card);
        }
    }

    public void Clear ()
    {
        ownedCards.Items.Clear();
        ownedPlanets.Items.Clear();
        ownedStars.Items.Clear();
    }
}
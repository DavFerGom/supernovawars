﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Data bases/Lists/Cards")]
public class CardsRuntimeSet : RuntimeSet<Card>
{
    [SerializeField]
    private SeenObjects CardsSeen;

    public override void Add (Card t)
    {
        CardsSeen.Discover(t.Data.ID);
        base.Add(t);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Lineups/Manager")]
public class LineupsManager : ScriptableObject
{
    [SerializeField]
    private Lineup playersLineup;

    public Lineup[] lineups = new Lineup[3];
    //TODO: Three lineups. Events in lineup changes.
    [SerializeField]
    private int currentLineupIndex = 0;
    public Lineup current
    {
        get
        {
            return lineups[currentLineupIndex];
        }
        set
        {
            for (int i = 0; i < lineups.Length; i++)
            {
                if (value.Equals(lineups[i]))
                {
                    currentLineupIndex = i;
                    playersLineup.Init(lineups[i]);
                    return;
                }
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Data bases/Lists/Planets")]
public class PlanetsRuntimeSet : RuntimeSet<Planet>
{
    [SerializeField]
    private SeenObjects PlanetsSeen;

    public override void Add (Planet t)
    {
        PlanetsSeen.Discover(t.Data.ID);
        base.Add(t);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Data bases/Lists/Stars")]
public class StarsRuntimeSet : RuntimeSet<Star>
{
    [SerializeField]
    private SeenObjects StarsSeen;

    public override void Add (Star t)
    {
        StarsSeen.Discover(t.Data.ID);
        base.Add(t);
    }
}
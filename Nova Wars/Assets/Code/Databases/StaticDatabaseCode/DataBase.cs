﻿using System;
using System.Linq;
using LitJson;
using UnityEngine;

[CreateAssetMenu(menuName = "Data bases/Static data base")]
[Serializable]
public class DataBase : ScriptableObject
{
    [SerializeField]
    private string version = "0.0.0.1";
    [SerializeField]
    public CardData[] Cards;
    [SerializeField]
    public StarData[] Stars;
    [SerializeField]
    public PlanetData[] Planets;
    [SerializeField]
    private CardTypes currentCardTypes;
    [SerializeField]
    private RarityLevels rarityLevels;
    [SerializeField]
    private AstronomicalTypes currentAstronomicalTypes;

    public void InitDataBase ()
    {
        Version = "0.0.0.1";
        Cards = new CardData[] { CardData.CreateInstance() };
        Stars = new StarData[] { StarData.CreateInstance() };
        Planets = new PlanetData[] { PlanetData.CreateInstance() };
    }
    /// <summary>
    /// Constructor used to load json data base
    /// </summary>
    /// <param name="myJsonData"></param>
    public void InitDataBase (JsonData myJsonData)
    {
        Version = myJsonData["Version"].ToString();

        JsonData myStuff = myJsonData["Cards"];
        
        Cards = new CardData[myStuff.Count];
        for (int i = 0; i < Cards.Length; i++)
        {
            Cards[i] = CardData.CreateInstance(myStuff[i]["Name"].ToString(),
                                               myStuff[i]["ID"].ToString(),
                                               myStuff[i]["Description"].ToString(),
                                               ( myStuff[i]["Type"] == null ) ? null : currentCardTypes.Types[currentCardTypes.GetIndex(myStuff[i]["Type"]["name"].ToString())],
                                               ( myStuff[i]["Rarity"] == null ) ? null : rarityLevels.Types[rarityLevels.GetIndex(myStuff[i]["Rarity"]["name"].ToString())]);
        }

        myStuff = myJsonData["Stars"];

        Stars = new StarData[myStuff.Count];
        for (int i = 0; i < Stars.Length; i++)
        {
            StarStats startStats = new StarStats(byte.Parse(myStuff[i]["Stats"]["Life"].ToString()),
                                            sbyte.Parse(myStuff[i]["Stats"]["Size"].ToString()),
                                            double.Parse(myStuff[i]["Stats"]["Cooldown"].ToString()));

            StarStats[] levelStats = new StarStats[9];
            for (int e = 0; e < levelStats.Length; e++)
            {
                levelStats[e] = new StarStats(
                                            byte.Parse(myStuff[i]["GrowthPerLevel"][e]["Life"].ToString()),
                                            sbyte.Parse(myStuff[i]["GrowthPerLevel"][e]["Size"].ToString()),
                                            double.Parse(myStuff[i]["GrowthPerLevel"][e]["Cooldown"].ToString()));
            }

            Stars[i] = StarData.CreateInstance(myStuff[i]["Name"].ToString(),
                                               myStuff[i]["ID"].ToString(),
                                               myStuff[i]["Description"].ToString(),
                                               ( myStuff[i]["Type"] == null ) ? null : currentAstronomicalTypes.Types[currentAstronomicalTypes.GetIndex(myStuff[i]["Type"]["name"].ToString())],
                                               ( myStuff[i]["Rarity"] == null ) ? null : rarityLevels.Types[rarityLevels.GetIndex(myStuff[i]["Rarity"]["name"].ToString())],
                                               ( myStuff[i]["Skill"] == null ) ? null : Resources.Load<StarSkill>(string.Format("Stars/Skills/{0}", myStuff[i]["Skill"]["name"])),
                                               startStats,
                                               levelStats);
        }

        myStuff = myJsonData["Planets"];

        Planets = new PlanetData[myStuff.Count];
        for (int i = 0; i < Planets.Length; i++)
        {
            PlanetStats startStats = new PlanetStats(byte.Parse(myStuff[i]["Stats"]["Life"].ToString()),
                                               sbyte.Parse(myStuff[i]["Stats"]["Size"].ToString()),
                                               byte.Parse(myStuff[i]["Stats"]["AmmunitionLimit"].ToString()),
                                               double.Parse(myStuff[i]["Stats"]["Cooldown"].ToString()));

            PlanetStats[] levelStats = new PlanetStats[9];
            for (int e = 0; e < levelStats.Length; e++)
            {
                levelStats[e] = new PlanetStats(byte.Parse(myStuff[i]["GrowthPerLevel"][e]["Life"].ToString()),
                                               sbyte.Parse(myStuff[i]["GrowthPerLevel"][e]["Size"].ToString()),
                                               byte.Parse(myStuff[i]["GrowthPerLevel"][e]["AmmunitionLimit"].ToString()),
                                               double.Parse(myStuff[i]["GrowthPerLevel"][e]["Cooldown"].ToString()));
            }

            Asteroid[] asteroids = new Asteroid[10];
            for (int e = 0; e < asteroids.Length; e++)
            {
                asteroids[e] = new Asteroid(byte.Parse(myStuff[i]["AsteroidsPerLevel"][e]["Life"].ToString()),
                                               sbyte.Parse(myStuff[i]["AsteroidsPerLevel"][e]["Size"].ToString()),
                                               byte.Parse(myStuff[i]["AsteroidsPerLevel"][e]["Speed"].ToString()),
                                               byte.Parse(myStuff[i]["AsteroidsPerLevel"][e]["Damage"].ToString()));
            }

            Planets[i] = PlanetData.CreateInstance(myStuff[i]["Name"].ToString(),
                                               myStuff[i]["ID"].ToString(),
                                               myStuff[i]["Description"].ToString(),
                                               byte.Parse(myStuff[i]["OverridenSkillCost"].ToString()),
                                               ( myStuff[i]["Type"] == null ) ? null : currentAstronomicalTypes.Types[currentAstronomicalTypes.GetIndex(myStuff[i]["Type"]["name"].ToString())],
                                               ( myStuff[i]["Rarity"] == null ) ? null : rarityLevels.Types[rarityLevels.GetIndex(myStuff[i]["Rarity"]["name"].ToString())],
                                               ( myStuff[i]["Skill"] == null ) ? null : Resources.Load<PlanetSkill>(string.Format("Planets/Skills/{0}", myStuff[i]["Skill"]["name"])),
                                               startStats,
                                               levelStats,
                                               asteroids);

        }
    }

    public void InitDataBase (TextAsset myJsonDataTextAsset)
    {
        InitDataBase(JsonMapper.ToObject(myJsonDataTextAsset.text));
    }
    
    public string Version
    {
        get
        {
            return version;
        }

        set
        {
            version = value;
        }
    }
    
    public string GetPlanetID (string name)
    {
        return Planets.First(x => name.Equals(x.Name)).ID;
    }

    public string GetStarID (string name)
    {
        return Stars.First(x => name.Equals(x.Name)).ID;
    }

    public string GetCardID (string name)
    {
        return Cards.First(x => name.Equals(x.Name)).ID;
    }

    public object GetObjectData (string ID)
    {
        switch (ID[0])
        {
            case '0':
                return Cards.First(x => ID.Equals(x.ID));
            case '1':
                return Stars.First(x => ID.Equals(x.ID));
            case '2':
                return Planets.First(x => ID.Equals(x.ID));
            default:
                return null;
        }
    }
}
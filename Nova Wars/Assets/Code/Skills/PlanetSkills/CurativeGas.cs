﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Planet Skill/CurativeGas")]
public class CurativeGas : PlanetSkill
{
    [SerializeField]
    private float lifePercentageRecovered = .5f;
    private Health[] otherPlanetsHealth = new Health[2];

    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);

        OnSkillUsed += RecoverLife;
        
        PlanetSkillComponent[] planetSkillComponents = sender.transform.parent.GetComponentsInChildren<PlanetSkillComponent>();
        for (int i = 0, e = 0; e < otherPlanetsHealth.Length; e++)
        {
            if (planetSkillComponents[e] != sender)
            {
                otherPlanetsHealth[i++] = planetSkillComponents[e].GetComponentInChildren<Health>();
            }
        }
    }

    private void RecoverLife (params MonoBehaviour[] sufferer)
    {
        //CurativeGas: hace que el resto de planetas aliados se curen un 50% de su vida total
        Debug.Log("Healing");

        for (int i = 0; i < otherPlanetsHealth.Length; i++)
        {
            int lifeToAdd = Mathf.RoundToInt(-otherPlanetsHealth[i].maxLife * lifePercentageRecovered);
            Debug.Log(lifeToAdd.ToString());
            otherPlanetsHealth[i].LoseLife(lifeToAdd);
        }
    }
}
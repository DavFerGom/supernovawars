﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Planet Skill/Sniper")]
public class Sniper : PlanetSkill
{
    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);
        
        OnSkillUsed += SniperShoot;
    }

    public void SniperShoot (params MonoBehaviour[] sufferer)
    {
        //Sniper: lanza un meteorito indestructible que causa daño extra (+2)
        Debug.Log("Asteroid launched");
        AsteroidMovement newAsteroid = sender.asteroidsPool.GetAsteroid(true);

        newAsteroid.GetComponentInChildren<Damage>().Initialize((byte) (newAsteroid.GetComponentInChildren<Damage>().damage + 2));

        newAsteroid.transform.position = sender.transform.position + ( Vector3.up * .7f );

        newAsteroid.Launch(Vector3.up);
    }
}
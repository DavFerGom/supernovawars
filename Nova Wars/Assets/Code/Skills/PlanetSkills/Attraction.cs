﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Planet Skill/Atraction")]
public class Attraction : PlanetSkill
{
    [SerializeField]
    private Mesh attractionFieldMesh;
    private MeshCollider attractionFieldTrigger;
    [SerializeField]
    private LayerMask layersAttractable; //Not working
    [SerializeField]
    private float range = 4f;
    [SerializeField]
    private float attractionForze = 300f;
    [SerializeField]
    private float timeUntilStop = 6f;

    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);

        OnSkillUsed += Attract;

        attractionFieldTrigger = new GameObject("Attraction", typeof(MeshCollider), typeof(AttractItems)).GetComponent<MeshCollider>();
        attractionFieldTrigger.gameObject.SetActive(false);
        attractionFieldTrigger.gameObject.layer = sender.gameObject.layer;

        attractionFieldTrigger.sharedMesh = attractionFieldMesh;
        attractionFieldTrigger.isTrigger = attractionFieldTrigger.convex = true;
        attractionFieldTrigger.transform.SetParent(sender.transform, false);
        attractionFieldTrigger.transform.localScale *= range;

        attractionFieldTrigger.GetComponent<AttractItems>().Initialize(layersAttractable, attractionForze);
    }

    public void Attract (params MonoBehaviour[] sufferer)
    {
        //Atraction: atrae todos los meteoritos enemigos hacia él durante 6 segundos
        Debug.Log("Attraction field running");
        attractionFieldTrigger.gameObject.SetActive(true);

        sender.StartCoroutine(Delay(timeUntilStop));
    }

    private IEnumerator Delay (float timeUntilStop)
    {
        yield return new WaitForSeconds(timeUntilStop);
        StopAttracting();
    }

    private void StopAttracting ()
    {
        attractionFieldTrigger.gameObject.SetActive(false);
    }
}
﻿using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Planet Skill/Quid Pro Quo")]
public class QuidProQuo : PlanetSkill
{
    [SerializeField]
    private float lifePercentageRecovered = .5f;
    private Health starHealth;

    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);

        OnSkillUsed += RecoverLife;

        starHealth = sender.transform.parent.GetComponentInChildren<StarComponent>().transform.GetComponentInChildren<Health>();
    }

    private void RecoverLife (params MonoBehaviour[] sufferer)
    {
        int lifeToAdd = Mathf.RoundToInt(-starHealth.maxLife * lifePercentageRecovered);
        Debug.Log(lifeToAdd.ToString());
        starHealth.LoseLife(lifeToAdd);
    }
}
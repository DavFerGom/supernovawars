﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Planet Skill/Ignored")]
public class Ignored : PlanetSkill
{
    [SerializeField]
    private float timeUntilStop = 5f;
    private Collider planetCollider;

    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);

        OnSkillUsed += Ignore;

        planetCollider = sender.transform.Find("Collider").GetComponent<Collider>();
    }

    public void Ignore (params MonoBehaviour[] sufferer)
    {
        //Ignored: no es golpeable durante 5 segundos
        Debug.Log("Being ignored");
        planetCollider.enabled = false;

        sender.StartCoroutine(Delay(timeUntilStop));
    }

    private IEnumerator Delay (float timeUntilStop)
    {
        yield return new WaitForSeconds(timeUntilStop);
        StopBeingIgnored();
    }

    private void StopBeingIgnored ()
    {
        planetCollider.enabled = true;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Planet Skill/Empty")]
public class PlanetSkill : GenericSkill
{
    [System.NonSerialized]
    protected PlanetSkillComponent sender;

    [SerializeField]
    private byte cost = 5;
    public byte Cost { get { return cost; } }
    [System.NonSerialized]
    private static float cooldownTime = .04f;
    [System.NonSerialized]
    protected bool couldBeUsed = true;
    [System.NonSerialized]
    private bool cooldownTimeRunning = false;

    public override event MonoBehaviourParamsDelegate OnSkillUsed;
    public override event StandardDelegate OnSkillBecomeReady;
    public event FloatParamDelegate RefreshCooldown;
    
    public override bool IsReady ()
    {
        return couldBeUsed && !cooldownTimeRunning;
    }
    
    public override void UseSkill (params MonoBehaviour[] sufferer)
    {
        if (IsReady())
        {
            if (OnSkillUsed != null)
            {
                couldBeUsed = false;
                OnSkillUsed(sufferer);
            }

            if (sender != null)
            {
                sender.StartCoroutine(Cooldown());
            }
            else
            {
                Debug.LogError("Sender was not initialized.");
            }
        }
        else
        {
            Debug.Log(string.Format("{0} not ready.", name));
        }
        //Debug.Log(string.Format("{0} is being used in {1}.", name, componentToSufferAbility.name), componentToSufferAbility.gameObject);
    }

    public override void InitSkill (MonoBehaviour sender)
    {
        try
        {
            this.sender = (PlanetSkillComponent) sender;
        }
        catch (System.InvalidCastException)
        {
            Debug.LogError(string.Format("{0} could not be casted to PlanetSkillComponent...", sender.GetType()));
        }
    }

    public void SetReady ()
    {
        couldBeUsed = true;
    }

    protected override void OnDestroy ()
    {
        sender = null;
    }
    
    private void CheckIfReady ()
    {
        if (couldBeUsed && OnSkillBecomeReady != null)
            OnSkillBecomeReady();
    }

    protected IEnumerator Cooldown ()
    {
        float timer = 0f;
        float inverseCooldownTime = 1f / cooldownTime;
        cooldownTimeRunning = true;

        do
        {
            timer += Time.deltaTime;
            if (RefreshCooldown != null)
                RefreshCooldown(timer * inverseCooldownTime);
            yield return null;
        } while (timer < cooldownTime);

        CheckIfReady();
        cooldownTimeRunning = false;
    }
}
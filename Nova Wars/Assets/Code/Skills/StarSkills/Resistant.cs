﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Star Skill/Resistant")]
public class Resistant : StarSkill
{
    [SerializeField]
    private float timeUntilStop = 30f;
    private EmptyAsteroid[] asteroidsToChange = new EmptyAsteroid[3];
    private byte[] formerLife = new byte[3];

    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);

        OnSkillUsed += Resist;

        for (int i = 0; i < 3; i++)
        {
            asteroidsToChange[i] = sender.transform.parent.GetComponentsInChildren<AsteroidsPool>()[i].AsteroidsStats;
        }
    }

    private void Resist (params MonoBehaviour[] sufferer)
    {
        //Resistant: tus asteroides tienen +1 durabilidad durante 30 segundos

        for (int i = 0; i < asteroidsToChange.Length; i++)
        {
            formerLife[i] = asteroidsToChange[i].stats.Life;
            asteroidsToChange[i].ChangeLife((byte)(formerLife[i] + 1));
        }

        sender.StartCoroutine(Delay(timeUntilStop));
    }

    private IEnumerator Delay (float timeUntilStop)
    {
        yield return new WaitForSeconds(timeUntilStop);
        StopAttracting();
    }

    private void StopAttracting ()
    {
        for (int i = 0; i < asteroidsToChange.Length; i++)
        {
            asteroidsToChange[i].ChangeLife(formerLife[i]);
        }
    }

    private void OnDisable ()
    {

        for (int i = 0; i < 3; i++)
        {
            asteroidsToChange[i] = null;
            formerLife[i] = 0;
        }
    }
}
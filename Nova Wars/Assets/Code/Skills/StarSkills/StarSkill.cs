﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Star Skill/Empty")]
public class StarSkill : GenericSkill
{
    //TODO: Replace MonoBehaviour with StarSkill component
    [NonSerialized]
    protected StarSkillComponent sender;

    [SerializeField]
    protected float cooldownTime = 5f;
    [NonSerialized]
    protected bool couldBeUsed = true;

    public override event MonoBehaviourParamsDelegate OnSkillUsed;
    public override event StandardDelegate OnSkillBecomeReady;
    public event FloatParamDelegate RefreshCooldown;

    public override bool IsReady ()
    {
        return couldBeUsed;
    }
    
    public override void UseSkill (params MonoBehaviour[] sufferer)
    {
        if (IsReady())
        {
            if (OnSkillUsed != null)
            {
                couldBeUsed = false;
                OnSkillUsed(sufferer);
            }

            if (sender != null)
            {
                sender.StartCoroutine(Cooldown());
            }
            else
            {
                Debug.LogError("Sender was not initialized.");
            }
        }
        else
        {
            Debug.Log(string.Format("{0} not ready.", name));
        }
        //Debug.Log(string.Format("{0} is being used in {1}.", name, componentToSufferAbility.name), componentToSufferAbility.gameObject);
    }

    public override void InitSkill (MonoBehaviour sender)
    {
        try
        {
            this.sender = (StarSkillComponent) sender;
        }
        catch (InvalidCastException)
        {
            Debug.LogError(string.Format("{0} is not cuold not be casted to SkillComponent...", sender.GetType()));
            return;
        }

        OnSkillBecomeReady += SetReady;
        //RefreshCooldown += DebugTimeToBePrepared;
    }

    protected override void OnDestroy ()
    {
        sender = null;

        OnSkillBecomeReady -= SetReady;
        //RefreshCooldown -= DebugTimeToBePrepared;
    }

    /*private void DebugTimeToBePrepared (float x)
    {
        Debug.Log(string.Format("{0} would be available again in {1}.", name, x.ToString("0")));
    }*/
    
    private void SetReady ()
    {
        couldBeUsed = true;
    }

    protected IEnumerator Cooldown ()
    {
        float timer = 0f;
        float inverseCooldownTime = 1f / cooldownTime;

        while (timer < cooldownTime)
        {
            timer += Time.deltaTime;
            if (RefreshCooldown != null)
                RefreshCooldown(timer * inverseCooldownTime);
            yield return null;
        }

        if (OnSkillBecomeReady != null)
            OnSkillBecomeReady();
    }
}
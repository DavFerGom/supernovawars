﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO: Show effect from scriptable

[CreateAssetMenu(menuName = "Skills/Star Skill/Pomatum")]
public class Pomatum : StarSkill
{
    [SerializeField]
    private Mesh shieldFieldMesh;
    private MeshCollider shieldFieldTrigger;
    [SerializeField]
    private LayerMask layersAttractable; //Not working
    [SerializeField]
    private float range = 4f;
    [SerializeField]
    private float timeUntilStop = 6f;

    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);

        OnSkillUsed += DeployPomatum;

        shieldFieldTrigger = new GameObject("Pomatum", typeof(MeshCollider), typeof(AttractItems)).GetComponent<MeshCollider>();
        shieldFieldTrigger.gameObject.SetActive(false);
        shieldFieldTrigger.gameObject.layer = sender.gameObject.layer;

        shieldFieldTrigger.sharedMesh = shieldFieldMesh;
        shieldFieldTrigger.isTrigger = shieldFieldTrigger.convex = true;
        shieldFieldTrigger.transform.SetParent(sender.transform, false);
        shieldFieldTrigger.transform.localScale *= range;
    }

    private void DeployPomatum (params MonoBehaviour[] sufferer)
    {
        //Pomatum: escudo antimeteoritos durante 8 segundos
        shieldFieldTrigger.gameObject.SetActive(true);

        sender.StartCoroutine(Delay(timeUntilStop));
    }

    private IEnumerator Delay (float timeUntilStop)
    {
        yield return new WaitForSeconds(timeUntilStop);
        StopAttracting();
    }

    private void StopAttracting ()
    {
        shieldFieldTrigger.gameObject.SetActive(false);
    }
}
﻿/*
 How to use it:

    Every skill will implement its own behaviour. If you want to use them,
    you will need an empty skill in assets in which current skill will
    be copied, so the skill used is the empty after been initialized. This
    way, you could reference same asset in all object depending on it as UI
    or MonoBehaviours.
     
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GenericSkill : ScriptableObject
{
    public abstract bool IsReady();
    public abstract void UseSkill (params MonoBehaviour[] sufferer);
    public abstract void InitSkill (MonoBehaviour sender);
    protected abstract void OnDestroy ();
    public abstract event MonoBehaviourParamsDelegate OnSkillUsed;
    public abstract event StandardDelegate OnSkillBecomeReady;
}
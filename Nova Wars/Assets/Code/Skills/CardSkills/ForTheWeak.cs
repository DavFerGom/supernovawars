﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Card Skill/ForTheWeak")]
public class ForTheWeak : CardSkill
{
    private Health[] ownPlanetsHealth;

    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);
        OnSkillUsed += UseForTheWeak;
        
        PlanetComponent[] planetComponents = sender.transform.parent.GetComponentsInChildren<PlanetComponent>();
        ownPlanetsHealth = new Health[planetComponents.Length];

        for (int i = 0; i < planetComponents.Length; i++)
        {
            ownPlanetsHealth[i] = planetComponents[i].GetComponentInChildren<Health>();
        }
    }
    
    private void UseForTheWeak (params MonoBehaviour[] sufferer)
    {
        //Cura totalmente al planeta con menor porcentaje de vida

        Health planetToHeal = ownPlanetsHealth[0];

        for (int i = 1; i < ownPlanetsHealth.Length; i++)
        {
            if (ownPlanetsHealth[i].currentLife > 0 && ( ownPlanetsHealth[i].currentLife * ownPlanetsHealth[i].inverseMaxLife ) <= ( planetToHeal.currentLife * planetToHeal.inverseMaxLife ))
                planetToHeal = ownPlanetsHealth[i];
        }

        if (planetToHeal.currentLife > 0)
            planetToHeal.LoseLife(-planetToHeal.maxLife);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Card Skill/AsteroidsEverywhere")]
public class AsteroidsEverywhere : CardSkill
{
    private List<AsteroidsPool> planetPools = new List<AsteroidsPool>();

    [SerializeField]
    private SingleAstronomicalType type;

    [SerializeField]
    private float timeWorking = 10f;

    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);
        OnSkillUsed += UseAsteroidsEverywhere;

        planetPools.Clear();
        PlanetComponent[] planetComponents = FindObjectsOfType<PlanetComponent>();

        for (int i = 0; i < planetComponents.Length; i++)
        {
            if (planetComponents[i].ScenePlanet.currentPlanet.Data.Type == type)
            {
                planetPools.Add(planetComponents[i].AsteroidsPool);
            }
        }
    }

    private void UseAsteroidsEverywhere (params MonoBehaviour[] sufferer)
    {
        //Planetas rocosos sin limite de munición durante 10 segundos
        for (int i = 0; i < planetPools.Count; i++)
        {
            planetPools[i].SetUnlimitedAsteroids();
        }

        sender.StartCoroutine(WaitUnitlBackToNormal());
    }

    private IEnumerator WaitUnitlBackToNormal ()
    {
        yield return new WaitForSeconds(timeWorking);

        BackToNormal();
    }

    private void BackToNormal ()
    {
        for (int i = 0; i < planetPools.Count; i++)
        {
            planetPools[i].LimiteAsteroids();
        }
    }
}
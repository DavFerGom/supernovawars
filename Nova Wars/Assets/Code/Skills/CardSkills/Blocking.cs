﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Card Skill/Blocking")]
public class Blocking : CardSkill
{
    [SerializeField]
    private float timeWorking = 25f;

    [SerializeField]
    private SingleCardType type;
    
    private List<CardComponent> cardsOfType = new List<CardComponent>();

    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);
        OnSkillUsed += UseBlocking;

        CardComponent[] cardComponents = FindObjectsOfType<CardComponent>();
        cardsOfType.Clear();

        for (int i = 0; i < cardComponents.Length; i++)
        {
            if (cardComponents[i].sceneCardType == type)
            {
                cardsOfType.Add(cardComponents[i]);
            }
        }
    }

    private void UseBlocking (params MonoBehaviour[] sufferer)
    {
        //Carta (x tipo) inutilizado durante 25 segundos
        for (int i = 0; i < cardsOfType.Count; i++)
        {
            if (cardsOfType[i].isActiveAndEnabled)
                cardsOfType[i].enabled = false;
            else
                cardsOfType.RemoveAt(i--);
        }

        sender.StartCoroutine(WaitUnitlBackToNormal());
    }

    private IEnumerator WaitUnitlBackToNormal ()
    {
        yield return new WaitForSeconds(timeWorking);

        for (int i = 0; i < cardsOfType.Count; i++)
        {
            cardsOfType[i].enabled = false;
        }
    }
}
﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Card Skill/Empty")]
public class CardSkill : GenericSkill
{
    [System.NonSerialized]
    protected CardComponent sender;

    [System.NonSerialized]
    protected bool wasUsed = false;

    public override event MonoBehaviourParamsDelegate OnSkillUsed;
    public override event StandardDelegate OnSkillBecomeReady;

    public override bool IsReady ()
    {
        return !wasUsed;
    }

    public override void UseSkill (params MonoBehaviour[] sufferers)
    {
        if (IsReady())
        {
            if (OnSkillUsed != null)
            {
                wasUsed = true;
                OnSkillUsed(sufferers);
            }
        }
        else
        {
            Debug.Log(string.Format("{0} not ready.", name));
        }
    }

    /// <summary>
    /// Override this to add especific functions to OnSkillUsed
    /// </summary>
    /// <param name="abilityToCopy">Template to get initial values</param>
    public override void InitSkill (MonoBehaviour sender)
    {
        try
        {
            this.sender = (CardComponent) sender;
        }
        catch (System.InvalidCastException)
        {
            Debug.LogError(string.Format("{0} could not be casted to CardComponent...", sender.GetType()));
        }

        OnSkillBecomeReady += SetReady;

        OnSkillBecomeReady();
    }

    protected override void OnDestroy ()
    {
        OnSkillBecomeReady -= SetReady;
    }

    private void SetReady ()
    {
        wasUsed = false;
    }
}
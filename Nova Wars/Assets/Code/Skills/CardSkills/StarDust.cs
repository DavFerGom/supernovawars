﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Card Skill/StarDust")]
public class StarDust : CardSkill
{
    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);
        OnSkillUsed += UseStarDust;

    }

    private void UseStarDust (params MonoBehaviour[] sufferer)
    {
        //Fuerza el evento de tipo polvo de estrellas
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Skills/Card Skill/Slowness")]
public class Slowness : CardSkill
{
    [SerializeField]
    private float speedMultiplier = .5f;
    [SerializeField]
    private float timeToRecover = 20f;

    private string enemy = "None";
    private EmptyAsteroid[] asteroidsToSlow;

    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);
        OnSkillUsed += UseSlowness;

        enemy = ( sender.tag.Contains("Player") ) ? "Enemy" : "Player";

        foreach (LineupInitializer initializer in FindObjectsOfType<LineupInitializer>())
        {
            if (initializer != sender.transform.parent.GetComponent<LineupInitializer>())
            {
                AsteroidsPool[] asteroidsPools = initializer.GetComponentsInChildren<AsteroidsPool>();
                asteroidsToSlow = new EmptyAsteroid[asteroidsPools.Length];
                for (int i = 0; i < asteroidsToSlow.Length; i++)
                {
                    asteroidsToSlow[i] = asteroidsPools[i].AsteroidsStats;
                }
                break;
            }
        }
    }

    private void UseSlowness (params MonoBehaviour[] sufferer)
    {
        byte[] lastValues = new byte[asteroidsToSlow.Length];
        for (int i = 0; i < asteroidsToSlow.Length; i++)
        {
            lastValues[i] = asteroidsToSlow[i].stats.Speed;
            asteroidsToSlow[i].ChangeSpeed((byte)Mathf.RoundToInt(lastValues[i] * speedMultiplier));
        }

        sender.StartCoroutine(RestoreSpeed(lastValues));
    }

    private IEnumerator RestoreSpeed (byte[] lastValues)
    {
        yield return new WaitForSeconds(timeToRecover);

        for (int i = 0; i < lastValues.Length; i++)
        {
            asteroidsToSlow[i].ChangeSpeed((byte) Mathf.RoundToInt(lastValues[i]));
        }
    }
}
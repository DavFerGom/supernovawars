﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "HabitableAgain")]
public class HabitableAgain : CardSkill
{
    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);
        OnSkillUsed += UseHabitableAgain;
        
    }

    private void UseHabitableAgain (params MonoBehaviour[] sufferer)
    {
        //Cura totalmente un planeta
    }
}
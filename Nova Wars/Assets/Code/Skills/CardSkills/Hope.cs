﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Card Skill/Hope")]
public class Hope : CardSkill
{
    private List<Health> typePlanetsHealth = new List<Health>();

    [SerializeField]
    private SingleAstronomicalType type;

    [SerializeField]
    private float timeWorking = 10f;

    [SerializeField]
    private float timeBetweenCures = 1f;

    [SerializeField]
    private byte healAmount = 1;

    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);
        OnSkillUsed += UseHope;
    }
    private void UseHope (params MonoBehaviour[] sufferer)
    {
        //Planetas gaseosos regeneran 1 vida por segundo durante 10 segundos

        PlanetComponent[] planetComponents = FindObjectsOfType<PlanetComponent>();
        typePlanetsHealth.Clear();

        for (int i = 0; i < planetComponents.Length; i++)
        {
            if (planetComponents[i].ScenePlanet.currentPlanet.Data.Type == type && typePlanetsHealth[i].currentLife > 0)
            {
                typePlanetsHealth.Add(planetComponents[i].GetComponentInChildren<Health>());
            }
        }

        sender.StartCoroutine(CurePlanets());
    }

    private IEnumerator CurePlanets ()
    {
        if (typePlanetsHealth.Count == 0)
        {
            yield return null;
        }
        else
        {
            float timer = 0f;
            while (timer < timeWorking)
            {
                for (int i = 0; i < typePlanetsHealth.Count; i++)
                {
                    if (typePlanetsHealth[i].currentLife > 0)
                        typePlanetsHealth[i].LoseLife(-healAmount);

                }
                yield return new WaitForSeconds(timeBetweenCures);
                timer += timeBetweenCures;
            }
        }
    }
}
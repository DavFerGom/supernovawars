﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Card Skill/TrueGods")]
public class TrueGods : CardSkill
{
    private Health[] ownPlanetsHealth;

    [SerializeField]
    private float timeWorking = 5f;

    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);
        OnSkillUsed += UseTrueGods;
        
        PlanetComponent[] planetComponents = sender.transform.parent.GetComponentsInChildren<PlanetComponent>();
        ownPlanetsHealth = new Health[planetComponents.Length];

        for (int i = 0; i < planetComponents.Length; i++)
        {
            ownPlanetsHealth[i] = planetComponents[i].GetComponentInChildren<Health>();
        }
    }

    private void UseTrueGods (params MonoBehaviour[] sufferer)
    {
        //Inmunidad total de tus planetas durante 5 segundos

        for (int i = 0; i < ownPlanetsHealth.Length; i++)
        {
            if (ownPlanetsHealth[i].currentLife > 0)
            {
                ownPlanetsHealth[i].SetImmunity();
            }
        }

        sender.StartCoroutine(WaitUnitlBackToNormal());
    }

    private IEnumerator WaitUnitlBackToNormal ()
    {
        yield return new WaitForSeconds(timeWorking);

        for (int i = 0; i < ownPlanetsHealth.Length; i++)
        {
            ownPlanetsHealth[i].StopImmunity();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Card Skill/NoEvent")]
public class NoEvent : CardSkill
{
    public override void InitSkill (MonoBehaviour sender)
    {
        base.InitSkill(sender);
        OnSkillUsed += UseNoEvent;
    }

    private void UseNoEvent (params MonoBehaviour[] sufferer)
    {
        //Desaparece el evento actual
        Debug.Log("Stop current event.");
    }
}
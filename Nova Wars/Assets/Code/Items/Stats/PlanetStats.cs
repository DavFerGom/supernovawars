﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlanetStats : AstronomicalStats
{
    [SerializeField]
    protected byte ammunitionLimit;
    [SerializeField]
    protected double cooldown;
    public byte AmmunitionLimit
    {
        get
        {
            return ammunitionLimit;
        }
    }
    public double Cooldown
    {
        get
        {
            return cooldown;
        }
    }

    public PlanetStats (byte life = 5, sbyte size = 0, byte ammunitionLimit = 7, double cooldown = 0)
    {
        this.life = life;
        this.size = size;
        this.ammunitionLimit = ammunitionLimit;
        this.cooldown = cooldown;
    }

    public PlanetStats (PlanetStats planetStats) : this(planetStats.Life, planetStats.Size, planetStats.AmmunitionLimit, planetStats.Cooldown)
    {

    }

    public void Copy (PlanetStats planetToCopy)
    {
        life = planetToCopy.Life;
        size = planetToCopy.Size;
        ammunitionLimit = planetToCopy.AmmunitionLimit;
        cooldown = planetToCopy.Cooldown;
    }
}
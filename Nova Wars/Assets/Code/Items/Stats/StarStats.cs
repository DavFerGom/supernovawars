﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StarStats : AstronomicalStats
{
    [SerializeField]
    private double cooldown;
    public double Cooldown
    {
        get
        {
            return cooldown;
        }
    }

    public StarStats (byte life = 10, sbyte size = 0, double cooldown = 5)
    {
        this.life = life;
        this.size = size;
        this.cooldown = cooldown;
    }

    public StarStats (StarStats starStats) : this(starStats.Life, starStats.Size, starStats.Cooldown)
    {

    }

    public void Copy (StarStats starToCopy)
    {
        life = starToCopy.Life;
        size = starToCopy.Size;
        cooldown = starToCopy.Cooldown;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AstronomicalStats
{
    [SerializeField]
    protected byte life;
    [SerializeField]
    protected sbyte size;

    public byte Life
    {
        get
        {
            return life;
        }
    }
    public sbyte Size
    {
        get
        {
            return size;
        }
    }
}
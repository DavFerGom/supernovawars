﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Asteroid : AstronomicalStats
{
    [SerializeField]
    private byte speed = 0;
    [SerializeField]
    private byte damage = 1;
    public byte Speed
    {
        get
        {
            return speed;
        }
    }
    public byte Damage
    {
        get
        {
            return damage;
        }
    }

    public Asteroid (byte life = 1, sbyte size = 0, byte speed = 0, byte damage = 0)
    {
        this.life = life;
        this.size = size;
        this.speed = speed;
        this.damage = damage;
    }

    public Asteroid (Asteroid asteroidStats) : this(asteroidStats.Life, asteroidStats.Size, asteroidStats.Speed, asteroidStats.Damage)
    {

    }

    public void Copy (Asteroid asteroidToCopy)
    {
        life = asteroidToCopy.Life;
        size = asteroidToCopy.Size;
        speed = asteroidToCopy.Speed;
        damage = asteroidToCopy.Damage;
    }
}
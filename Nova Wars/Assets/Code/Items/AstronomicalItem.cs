﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstronomicalItem : Item
{
    public double Experience
    {
        private set;
        get;
    }
    public byte Level
    {
        private set;
        get;
    }
    public int Priority
    {
        private set;
        get;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : ScriptableObject
{
    virtual public ItemData Data { get; private set; }
    [SerializeField]
    public Skin CurrentSkin;
    
    public void Init ()
    {

    }
}
﻿using System;
using UnityEngine;

[Serializable]
public class CardData : ItemData
{
    /*[SerializeField]
    protected new SingleCardType type;
    public new SingleCardType Type
    {
        get
        {
            return type;
        }
    }*/
    [SerializeField]
    protected CardSkill skill;
    public CardSkill Skill
    {
        get
        {
            return skill;
        }
    }
    
    /*public CardData (string name = "Default", string ID = "00000", string description = "", SingleCardType type = null, SingleRarityLevel rarity = null, CardSkill skill = null)
    {
        this.name = name;
        _ID = ID;
        this.description = description;
        this.type = type;
        this.rarity = rarity;

        this.skill = skill;
    }
    */
    public void Copy (CardData cardToCopy)
    {
        Name = cardToCopy.Name;
        _ID = cardToCopy.ID;
        description = cardToCopy.Description;
        type = cardToCopy.Type;
        rarity = cardToCopy.Rarity;

        skill = cardToCopy.Skill;
    }

    protected void Init (string name = "Default", string ID = "00000", string description = "", SingleCardType type = null, SingleRarityLevel rarity = null, CardSkill skill = null)
    {
        base.Init(name, ID, description, type, rarity);

        this.skill = skill;
    }

    public static CardData CreateInstance (string name = "Default", string ID = "00000", string description = "", SingleCardType type = null, SingleRarityLevel rarity = null, CardSkill skill = null)
    {
        var data = new CardData(); // CreateInstance<CardData>();
        data.Init(name, ID, description, type, rarity, skill);
        return data;
    }
}
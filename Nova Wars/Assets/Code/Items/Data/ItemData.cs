﻿using System;
using UnityEngine;

[Serializable]
public class ItemData 
{
    [SerializeField]
    protected string _ID = "";
    [SerializeField]
    protected string description = "";
    [SerializeField]
    protected SingleType type;
    [SerializeField]
    protected SingleRarityLevel rarity;

    public string Name { protected set; get; }
    public string ID
    {
        get
        {
            return _ID;
        }
    }
    public string Description
    {
        get
        {
            return description;
        }
    }
    public SingleType Type
    {
        get
        {
            return type;
        }
    }
    public SingleRarityLevel Rarity
    {
        get
        {
            return rarity;
        }
    }

    public void Init (string name = "Default", string ID = "00000", string description = "", SingleType type = null, SingleRarityLevel rarity = null)
    {
        Name = name;
        _ID = ID;
        this.description = description;
        this.type = type;
        this.rarity = rarity;
    }

    public static ItemData CreateInstance (string name = "Default", string ID = "00000", string description = "", SingleType type = null, SingleRarityLevel rarity = null)
    {
        var data = new ItemData();// CreateInstance<ItemData>();
        data.Init(name, ID, description, type, rarity);
        return data;
    }
}
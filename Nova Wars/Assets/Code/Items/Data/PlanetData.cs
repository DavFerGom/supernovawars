﻿using System;
using UnityEngine;

[Serializable]
public class PlanetData : ItemData
{
    [SerializeField]
    protected PlanetStats stats = new PlanetStats();
    public PlanetStats Stats
    {
        get
        {
            return stats;
        }
    }
    [SerializeField]
    protected PlanetStats[] growthPerLevel = new PlanetStats[9];
    public PlanetStats[] GrowthPerLevel
    {
        get
        {
            return growthPerLevel;
        }
    }
    [SerializeField]
    protected Asteroid[] asteroidsPerLevel = new Asteroid[10];
    public Asteroid[] AsteroidsPerLevel
    {
        get
        {
            return asteroidsPerLevel;
        }
    }
    [SerializeField]
    protected PlanetSkill skill;
    public PlanetSkill Skill
    {
        get
        {
            return skill;
        }
    }
    [SerializeField]
    private byte overridenSkillCost;
    public byte OverridenSkillCost
    {
        get
        {
            return overridenSkillCost;
        }
    }

    /*public PlanetData (string name = "Default", string ID = "20000", string description = "", SingleAstronomicalType type = null, SingleRarityLevel rarity = null, PlanetSkill skill = null, PlanetStats planetBasicStats = null, PlanetStats[] planetLevels = null, Asteroid[] asteroidsPerLevel = null)
    {
        this.name = name;
        _ID = ID;
        this.description = description;
        this.type = type;
        this.rarity = rarity;
        this.skill = skill;

        if (planetBasicStats != null)
            stats.Copy(planetBasicStats);

        for (byte i = 0; i < growthPerLevel.Length; i++)
        {
            growthPerLevel[i] = planetLevels != null ? new PlanetStats(planetLevels[i]) : new PlanetStats();
        }

        for (byte i = 0; i < this.asteroidsPerLevel.Length; i++)
        {
            this.asteroidsPerLevel[i] = asteroidsPerLevel != null ? new Asteroid(asteroidsPerLevel[i]) : new Asteroid();
        }
    }
    */
    public void Copy (PlanetData planetToCopy)
    {
        Name = planetToCopy.Name;
        _ID = planetToCopy.ID;
        description = planetToCopy.Description;
        type = planetToCopy.Type;
        rarity = planetToCopy.Rarity;
        skill = planetToCopy.skill;
        overridenSkillCost = planetToCopy.overridenSkillCost;
        stats.Copy(planetToCopy.stats);
        for (byte i = 0; i < planetToCopy.growthPerLevel.Length; i++)
        {
            growthPerLevel[i].Copy(planetToCopy.GetLevelGrowth(i));
        }
        for (byte i = 0; i < asteroidsPerLevel.Length; i++)
        {
            asteroidsPerLevel[i].Copy(planetToCopy.asteroidsPerLevel[i]);
        }
    }

    public void Init (string name = "Default", string ID = "20000", string description = "", byte overridenSkillCost = 0, SingleAstronomicalType type = null, SingleRarityLevel rarity = null, PlanetSkill skill = null, PlanetStats planetBasicStats = null, PlanetStats[] planetLevels = null, Asteroid[] asteroidsPerLevel = null)
    {
        base.Init(name, ID, description, type, rarity);

        this.skill = skill;
        this.overridenSkillCost = overridenSkillCost;

        if (planetBasicStats != null)
            stats.Copy(planetBasicStats);

        for (byte i = 0; i < growthPerLevel.Length; i++)
        {
            growthPerLevel[i] = planetLevels != null && planetLevels[i] != null ? new PlanetStats(planetLevels[i]) : new PlanetStats();
        }

        for (byte i = 0; i < this.asteroidsPerLevel.Length; i++)
        {
            this.asteroidsPerLevel[i] = asteroidsPerLevel != null ? new Asteroid(asteroidsPerLevel[i]) : new Asteroid();
        }
    }

    public static PlanetData CreateInstance (string name = "Default", string ID = "20000", string description = "", byte overridenSkillCost = 0, SingleAstronomicalType type = null, SingleRarityLevel rarity = null, PlanetSkill skill = null, PlanetStats planetBasicStats = null, PlanetStats[] planetLevels = null, Asteroid[] asteroidsPerLevel = null)
    {
        var data = new PlanetData(); //CreateInstance<PlanetData>();
        data.Init(name, ID, description, overridenSkillCost, type, rarity, skill, planetBasicStats, planetLevels, asteroidsPerLevel);
        return data;
    }

    public PlanetStats GetLevelGrowth (byte level = 0)
    {
        return growthPerLevel[level];
    }

    public Asteroid GetLevelAsteroid (byte level = 0)
    {
        byte life = 0;
        sbyte size = 0;
        byte speed = 0;
        byte damage = 0;

        for (int i = 0; i <= level; i++)
        {
            life += asteroidsPerLevel[i].Life;
            size += asteroidsPerLevel[i].Size;
            speed += asteroidsPerLevel[i].Speed;
            damage += asteroidsPerLevel[i].Damage;
        }

        return new Asteroid(life, size, speed, damage);
    }

    public PlanetStats GetLevelStats (byte level = 0)
    {
        byte life = stats.Life;
        sbyte size = stats.Size;
        byte ammunitionLimit = stats.AmmunitionLimit;
        double cooldown = stats.Cooldown;

        for (int i = 0; i <= level; i++)
        {
            life += growthPerLevel[i].Life;
            size += growthPerLevel[i].Size;
            ammunitionLimit += growthPerLevel[i].AmmunitionLimit;
            cooldown += growthPerLevel[i].Cooldown;
        }

        return new PlanetStats(life, size, ammunitionLimit, cooldown);
    }
}
﻿using System;
using UnityEngine;

[Serializable]
public class StarData : ItemData
{
    [SerializeField]
    protected StarStats stats = new StarStats();
    public StarStats Stats
    {
        get
        {
            return stats;
        }
    }
    [SerializeField]
    protected StarStats[] growthPerLevel = new StarStats[9];
    public StarStats[] GrowthPerLevel
    {
        get
        {
            return growthPerLevel;
        }
    }
    [SerializeField]
    protected StarSkill skill;
    public StarSkill Skill
    {
        get
        {
            return skill;
        }
    }

    /*public StarData (string name = "Default", string ID = "10000", string description = "", SingleAstronomicalType type = null, SingleRarityLevel rarity = null, StarSkill skill = null, StarStats starBasicStats = null, StarStats[] starLevels = null)
    {
        this.name = name;
        _ID = ID;
        this.description = description;
        this.type = type;
        this.rarity = rarity;

        this.skill = skill;

        stats.Copy(starBasicStats ?? new StarStats());
        for (int i = 0; i < growthPerLevel.Length; i++)
        {
            growthPerLevel[i] = starLevels != null ? new StarStats(starLevels[i]) : new StarStats();
        }
    }
    */
    public void Copy (StarData starToCopy)
    {
        Name = starToCopy.Name;
        _ID = starToCopy.ID;
        description = starToCopy.Description;
        type = starToCopy.Type;
        rarity = starToCopy.Rarity;

        skill = starToCopy.Skill;

        stats.Copy(starToCopy.stats);
        for (byte i = 1; i < starToCopy.growthPerLevel.Length; i++)
        {
            growthPerLevel[i - 1].Copy(starToCopy.GetLevelGrowth(i));
        }
    }

    public void Init (string name = "Default", string ID = "20000", string description = "", SingleAstronomicalType type = null, SingleRarityLevel rarity = null, StarSkill skill = null, StarStats starBasicStats = null, StarStats[] starLevels = null)
    {
        base.Init(name, ID, description, type, rarity);

        this.skill = skill;

        stats.Copy(starBasicStats ?? new StarStats());
        for (int i = 0; i < growthPerLevel.Length; i++)
        {
            growthPerLevel[i] = starLevels != null ? new StarStats(starLevels[i]) : new StarStats();
        }
    }

    public static StarData CreateInstance (string name = "Default", string ID = "20000", string description = "", SingleAstronomicalType type = null, SingleRarityLevel rarity = null, StarSkill skill = null, StarStats starBasicStats = null, StarStats[] starLevels = null)
    {
        var data = new StarData();// CreateInstance<StarData>();
        data.Init(name, ID, description, type, rarity, skill, starBasicStats, starLevels);
        return data;
    }

    public StarStats GetLevelGrowth (byte level)
    {
        return growthPerLevel[level - 1];
    }
}
﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Object/Empty star")]
public class EmptyStar : ScriptableObject
{
    public Star currentStar;
    public event StarDelegate OnInitialized;
    
    public void Init (Star starToSet)
    {
        currentStar = Star.CreateInstance((StarData)starToSet.Data);

        if (OnInitialized != null)
            OnInitialized(starToSet);
    }

    private void OnDisable ()
    {
        currentStar = null;
    }

    public void UseSkill ()
    {
        Debug.Log(( currentStar.CurrentSkin == null ) ? "None" : currentStar.CurrentSkin.name);
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Object/Card")]
public class Card : Item
{
    [SerializeField]
    private CardData data;
    override public ItemData Data { get { return data; } }

    public void Copy (Card cardToUse)
    {
        data.Copy((CardData) cardToUse.Data);
    }

    public void Init (CardData data)
    {
        this.data = new CardData();
        this.data.Init();
        this.data.Copy(data);
    }

    public static Card CreateInstance (CardData data)
    {
        var instance = CreateInstance<Card>();
        instance.Init(data);
        return instance;
    }
}
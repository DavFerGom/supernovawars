﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Object/Star")]
public class Star : AstronomicalItem
{
    [SerializeField]
    private StarData data;
    override public ItemData Data { get { return data; } }

    public void Copy (Star starToUse)
    {
        data.Copy((StarData) starToUse.Data);
    }

    public void Init (StarData data)
    {
        this.data = new StarData();
        this.data.Init();
        this.data.Copy(data);
    }

    public static Star CreateInstance (StarData data)
    {
        var instance = CreateInstance<Star>();
        instance.Init(data);
        return instance;
    }
}
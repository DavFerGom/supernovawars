﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Lineups/Lineup")]
public class Lineup : ScriptableObject
{
    //TODO: ¿eventos para cuando cambie cada una de las posiciones?

	public void Init (Lineup lineup)
    {
        Clear();
        foreach (Planet planet in lineup.currentPlanets)
        {
            currentPlanets.Add(planet);
        }
        currentStar = lineup.currentStar;
        foreach (Card card in lineup.currentCards)
        {
            currentCards.Add(card);
        }
    }

    public List<Card> currentCards = new List<Card>(4);
    public List<Planet> currentPlanets = new List<Planet>(3);
    public Star currentStar;

    public void SetCard (Card card, byte position)
    {
        currentCards[position] = card;
    }

    public void SetPlanet (Planet planet, byte position)
    {
        currentPlanets[position] = planet;
    }

    public void SetStar (Star star)
    {
        currentStar = star;
    }

    public void Clear ()
    {
        currentStar = null;
        currentPlanets.Clear();
        currentCards.Clear();
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "EmptyAsteroid")]
public class EmptyAsteroid : ScriptableObject
{
    private Planet parentPlanet;

    public Asteroid stats { get; private set; }
    public event AsteroidDelegate OnInitialized;
    public event StandardDelegate OnAsteroidChanged;

    public void Initialize (Asteroid levelAsteroid, Planet parentPlanet)
    {
        stats = new Asteroid();
        this.parentPlanet = parentPlanet;

        stats.Copy(levelAsteroid);

        if (OnInitialized != null)
            OnInitialized(levelAsteroid);
    }

    public void SetSkin (GameObject model)
    {
        ( (PlanetSkin) parentPlanet.CurrentSkin ).SetAsteroidSkin(model);
    }

    public void ChangeLife (byte newMaxLife)
    {
        stats.Copy(new Asteroid(newMaxLife, stats.Size, stats.Speed, stats.Damage));

        if (OnAsteroidChanged != null)
            OnAsteroidChanged();
    }

    public void ChangeSize (sbyte newMaxSize)
    {
        stats.Copy(new Asteroid(stats.Life, newMaxSize, stats.Speed, stats.Damage));

        if (OnAsteroidChanged != null)
            OnAsteroidChanged();
    }

    public void ChangeSpeed (byte newMaxSpeed)
    {
        stats.Copy(new Asteroid(stats.Life, stats.Size, newMaxSpeed, stats.Damage));

        if (OnAsteroidChanged != null)
            OnAsteroidChanged();
    }

    public void ChangeDamage (byte newMaxDamage)
    {
        stats.Copy(new Asteroid(stats.Life, stats.Size, stats.Speed, newMaxDamage));

        if (OnAsteroidChanged != null)
            OnAsteroidChanged();
    }


    private void OnDisable ()
    {
        stats = null;
    }
}
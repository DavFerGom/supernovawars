﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Object/Planet")]
public class Planet : AstronomicalItem
{
    [SerializeField]
    private PlanetData data;
    override public ItemData Data { get { return data; } }

    public void Copy (Planet planetToUse)
    {
        data.Copy((PlanetData) planetToUse.Data);
    }

    public void Init (PlanetData data)
    {
        this.data = new PlanetData();
        this.data.Init();
        this.data.Copy(data);
    }

    public static Planet CreateInstance (PlanetData data)
    {
        var instance = CreateInstance<Planet>();
        instance.Init(data);
        return instance;
    }
}
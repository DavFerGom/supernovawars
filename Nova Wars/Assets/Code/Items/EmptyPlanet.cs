﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Object/Empty planet")]
public class EmptyPlanet : ScriptableObject
{
    public Planet currentPlanet;
    public PlanetStats currentStats;

    public event PlanetDelegate OnInitialized;

    public void Init (Planet planetToSet)
    {
        currentPlanet = Planet.CreateInstance((PlanetData) planetToSet.Data);
        currentStats = ( (PlanetData) planetToSet.Data ).GetLevelStats(planetToSet.Level);

        if (OnInitialized != null)
            OnInitialized(planetToSet);
    }

    private void OnDisable ()
    {
        currentPlanet = null;
    }
}
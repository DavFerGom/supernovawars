﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Object/Empty card")]
public class EmptyCard : ScriptableObject
{
    public Card currentCard;
    public event CardDelegate OnInitialized;

    public void Init (Card cardToSet)
    {
        currentCard = Card.CreateInstance((CardData) cardToSet.Data);
        
        if (OnInitialized != null)
            OnInitialized(cardToSet);
    }

    private void OnDisable ()
    {
        currentCard = null;
    }

    public void UseSkill ()
    {
        CardSkill currenSkill = ( (CardData) currentCard.Data ).Skill;

        if(currenSkill != null)
        {
            currenSkill.UseSkill();
        }
    }
}
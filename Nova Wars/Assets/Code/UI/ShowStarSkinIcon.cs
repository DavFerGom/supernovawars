﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowStarSkinIcon : MonoBehaviour
{
    [SerializeField]
    private EmptyStar star;
    private Image iconImage;

    private void Awake ()
    {
        iconImage = GetComponent<Image>();
        star.OnInitialized += Initialize;
    }

    private void Initialize (Star currentStar)
    {
        iconImage.sprite = currentStar.CurrentSkin.icon;
    }
}
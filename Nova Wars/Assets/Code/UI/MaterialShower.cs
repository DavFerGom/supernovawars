﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MaterialShower : MonoBehaviour
{
    [SerializeField]
    private SingleAstronomicalType materialToShow;
    [SerializeField]
    private Materials materials;
    private TextMeshProUGUI myTextComponent;
    private string text { set { myTextComponent.text = value; } }

    public void Awake ()
	{
        materials[materialToShow].AmountChanged += ShowMaterialAmount;
        myTextComponent = transform.Find("Amount").GetComponent<TextMeshProUGUI>();

    }

    private void ShowMaterialAmount(int amount)
    {
        text = amount.ToString("0");
    }
}
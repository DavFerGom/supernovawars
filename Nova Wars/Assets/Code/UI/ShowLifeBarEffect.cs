﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowLifeBarEffect : MonoBehaviour
{
    [SerializeField]
    private Slider backgroundBar;
    [SerializeField]
    private float delay = 0.5f;
    [SerializeField]
    private float speed = 0.5f;
    
    public void OnLifeBarValueChanged (float floatParam)
    {
        if (backgroundBar.value > floatParam)
            StartCoroutine(ProgressiveSetNewValue(floatParam));
        else
            backgroundBar.value = floatParam;
    }

    private IEnumerator ProgressiveSetNewValue (float floatParam)
    {
        yield return new WaitForSeconds (delay);

        while (backgroundBar.value >= floatParam)
        {
            backgroundBar.value -= speed * Time.deltaTime;
            yield return null;
        }
    }
}
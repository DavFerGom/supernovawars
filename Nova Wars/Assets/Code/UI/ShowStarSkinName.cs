﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShowStarSkinName : MonoBehaviour
{
    [SerializeField]
    private EmptyStar star;
    private TMP_Text text;

	private void Awake ()
	{
        text = GetComponent<TMP_Text>();
        star.OnInitialized += Initialize;
	}

	private void Initialize (Star currentPlanet)
	{
        text.text = currentPlanet.CurrentSkin.name;
	}
}
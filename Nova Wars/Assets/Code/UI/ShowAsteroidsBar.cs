﻿using UnityEngine;
using UnityEngine.UI;

public class ShowAsteroidsBar : MonoBehaviour
{
    [SerializeField]
    private AsteroidsPool asteroids;
    [SerializeField]
    private Slider ammunitionBar;

    private void Awake ()
    {
        asteroids.OnAmountChanged += Asteroids_OnAmountChanged;
    }
    
    private void Asteroids_OnAmountChanged (float floatParam)
    {
        ammunitionBar.value = floatParam;
    }
}
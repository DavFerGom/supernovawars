﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowCardSkinIcon : MonoBehaviour
{
    [SerializeField]
    private EmptyCard card;
    private Image iconImage;

    private void Awake ()
    {
        iconImage = GetComponent<Image>();
        card.OnInitialized += Initialize;
    }

    private void Initialize (Card currentStar)
    {
        iconImage.sprite = currentStar.CurrentSkin.icon;
    }
}
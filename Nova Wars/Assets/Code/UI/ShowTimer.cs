﻿using System;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class ShowTimer : MonoBehaviour
{
    private TextMeshProUGUI myTextComponent;
    private string text
    {
        set
        {
            myTextComponent.text = value;
        }
    }

    [SerializeField]
    private Timer timer;

    private void Awake ()
    {
        myTextComponent = GetComponent<TextMeshProUGUI>() ?? gameObject.AddComponent<TextMeshProUGUI>();

        timer.OnTimeCounting += Timer_OnTimeCounting;
        timer.OnBeginTime += Timer_OnBeginTimer;
    }

    private void Timer_OnBeginTimer (float floatParam)
    {
        TimeSpan time = TimeSpan.FromSeconds(floatParam);
        text = string.Format("{0:0}:{1:00}", time.Minutes, time.Seconds);
    }

    private void Timer_OnTimeCounting (float floatParam)
    {
        TimeSpan time = TimeSpan.FromSeconds(floatParam);
        text = string.Format("{0:0}:{1:00}", time.Minutes, time.Seconds);
    }
}
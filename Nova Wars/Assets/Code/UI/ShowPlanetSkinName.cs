﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShowPlanetSkinName : MonoBehaviour
{
    [SerializeField]
    private EmptyPlanet planet;
    private TMP_Text text;

	private void Awake ()
	{
        text = GetComponent<TMP_Text>();
        planet.OnInitialized += Initialize;
	}

	private void Initialize (Planet currentPlanet)
	{
        text.text = currentPlanet.CurrentSkin.name;
	}
}
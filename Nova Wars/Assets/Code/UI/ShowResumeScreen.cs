﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowResumeScreen : MonoBehaviour
{
	private void Awake ()
	{
        gameObject.SetActive(false);

        GamePlayFlow.OnGameEnded += GamePlayFlow_OnGameEnded;
	}

    private void GamePlayFlow_OnGameEnded ()
    {
        gameObject.SetActive(true);
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IntegerToText : MonoBehaviour
{
    [SerializeField]
    private Integer scriptableFloat;
    private TextMeshProUGUI myTextComponent;
    private string text { set { myTextComponent.text = value; } }

    public void Awake ()
    {
        scriptableFloat.AmountChanged += ShowMaterialAmount;
        myTextComponent = transform.Find("Amount").GetComponent<TextMeshProUGUI>();
    }

    private void ShowMaterialAmount (int amount)
    {
        text = amount.ToString("0");
    }
}
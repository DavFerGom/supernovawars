﻿using UnityEngine;
using UnityEngine.UI;

public class ShowLifeBar : MonoBehaviour
{
    [SerializeField]
    private Health health;
    [SerializeField]
    private Slider lifeBar;

    private void Awake ()
    {
        health.OnHealthChanged += Health_OnHealthChanged;
        health.OnDeath += Health_OnDeath;
        if (health.GetType() == typeof(PlanetHealth))
            ((PlanetHealth) health).OnResurrection += Resurrect;
    }

    private void Resurrect ()
    {
        if (gameObject.tag.Contains("Player"))
            transform.parent.gameObject.SetActive(true);
        else
            gameObject.SetActive(true);
    }

    private void Health_OnDeath ()
    {
        if (gameObject.tag.Contains("Player"))
            transform.parent.gameObject.SetActive(false);
        else
            gameObject.SetActive(false);
    }

    private void Health_OnHealthChanged (float floatParam)
    {
        lifeBar.value = floatParam;
    }

    private void OnDestroy ()
    {
        health.OnHealthChanged -= Health_OnHealthChanged;
        health.OnDeath -= Health_OnDeath;
        if (health.GetType() == typeof(PlanetHealth))
            ( (PlanetHealth) health ).OnResurrection -= Resurrect;
    }
}
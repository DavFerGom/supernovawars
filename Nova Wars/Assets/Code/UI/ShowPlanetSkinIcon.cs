﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowPlanetSkinIcon : MonoBehaviour
{
    [SerializeField]
    private EmptyPlanet planet;
    private Image iconImage;

    private void Awake ()
    {
        iconImage = GetComponent<Image>();
        planet.OnInitialized += Initialize;
    }

    private void Initialize (Planet currentPlanet)
    {
        iconImage.sprite = currentPlanet.CurrentSkin.icon;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CollectionManager : MonoBehaviour
{
    [Header("Scriptables")]
    [SerializeField]
    private CraftFactory factory;
    [SerializeField]
    private Collection collection;
    [SerializeField]
    protected SeenObjects playerSeenItems;
    [Header("Prefabs")]
    [SerializeField]
    private GameObject itemUIPrefab;
    private List<GameObject> itemsUIPool = new List<GameObject>();
    [SerializeField]
    private GameObject prefabShadow;
    private List<GameObject> itemShadowsUIPool = new List<GameObject>();
    [Header("Panels")]
    [SerializeField]
    private RectTransform ownedPanel;
    [SerializeField]
    private RectTransform seenPanel;
    [Header("Databases")]
    [SerializeField]
    private StarsRuntimeSet allStars;
    [SerializeField]
    private PlanetsRuntimeSet allPlanets;
    [SerializeField]
    private CardsRuntimeSet allCards;

    public void Init ()
    {
        RefreshPlanets();
    }

    private void ShowNewItem (Item item)
    {
        GameObject uIPrefab = GetUIItem();

        //TODO: uIPrefab.GetComponent<Image>().sprite = item.CurrentSkin.icon ?? null;
        //uIPrefab.GetComponent<Image>().sprite = ( item.CurrentSkin != null ) ? item.CurrentSkin.icon ?? null : null;
        uIPrefab.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = item.name;
        uIPrefab.transform.Find("Level").gameObject.SetActive(!item.GetType().Equals(typeof(Card)));
        if (!item.GetType().Equals(typeof(Card)))
        {
            uIPrefab.transform.Find("Level").GetComponent<TextMeshProUGUI>().text = ((AstronomicalItem)item).Level.ToString();
        }
        uIPrefab.SetActive(true);
    }

    private void ShowNewShadow (Item item)
    {
        GameObject uIShadow = GetUIShadow();

        //TODO:uIShadow.GetComponent<Image>().sprite = item.CurrentSkin.icon;
        uIShadow.GetComponent<Image>().sprite = ( item.CurrentSkin != null) ? item.CurrentSkin.icon ?? null : null;
        uIShadow.GetComponent<Button>().onClick.RemoveAllListeners();
        uIShadow.GetComponent<Button>().onClick.AddListener(() => factory.CraftItem(item));

        uIShadow.SetActive(true);
    }

    private GameObject GetUIItem ()
    {
        GameObject uIItemToReturn = ( itemsUIPool.Count > 0 ) ? itemsUIPool.Find(x => !x.activeSelf) : null;

        if (uIItemToReturn != null)
            return uIItemToReturn;

        uIItemToReturn = Instantiate(itemUIPrefab, ownedPanel);
        uIItemToReturn.gameObject.SetActive(false);
        itemsUIPool.Add(uIItemToReturn);

        return uIItemToReturn;
    }

    private GameObject GetUIShadow ()
    {
        GameObject uIShadowToReturn = ( itemShadowsUIPool.Count > 0 ) ? itemShadowsUIPool.Find(x => !x.activeSelf) : null;

        if (uIShadowToReturn != null)
            return uIShadowToReturn;

        uIShadowToReturn = Instantiate(prefabShadow, seenPanel);
        uIShadowToReturn.gameObject.SetActive(false);
        itemShadowsUIPool.Add(uIShadowToReturn);

        return uIShadowToReturn;
    }

    private void HideAll ()
    {
        itemsUIPool.ForEach(x => x.SetActive(false));
        itemShadowsUIPool.ForEach(x => x.SetActive(false));
    }

    public void RefreshStars ()
    {
        HideAll();
        collection.ownedStars.Items.ForEach(x => ShowNewItem(x));

        for (int i = 0; playerSeenItems != null && i < playerSeenItems.objectsOwned.Count; i++)
        {
            if (playerSeenItems.objectsOwned[i].Substring(0, 1).Equals("1") && allStars.Items.Exists(x => x.Data.ID.Equals(playerSeenItems.objectsOwned[i])))
            {
                ShowNewShadow(allStars.Items.Find(x => x.Data.ID.Equals(playerSeenItems.objectsOwned[i])));
            }
        }
        LayoutRebuilder.ForceRebuildLayoutImmediate(ownedPanel);
        LayoutRebuilder.ForceRebuildLayoutImmediate(seenPanel);
    }

    public void RefreshPlanets ()
    {
        HideAll();
        collection.ownedPlanets.Items.ForEach(x => ShowNewItem(x));

        for (int i = 0; playerSeenItems != null && i < playerSeenItems.objectsOwned.Count; i++)
        {
            if (playerSeenItems.objectsOwned[i].Substring(0, 1).Equals("2") && allPlanets.Items.Exists(x => x.Data.ID.Equals(playerSeenItems.objectsOwned[i])))
            {
                ShowNewShadow(allPlanets.Items.Find(x => x.Data.ID.Equals(playerSeenItems.objectsOwned[i])));
            }
        }
        LayoutRebuilder.ForceRebuildLayoutImmediate(ownedPanel);
        LayoutRebuilder.ForceRebuildLayoutImmediate(seenPanel);
    }

    public void RefreshCards ()
    {
        HideAll();
        collection.ownedCards.Items.ForEach(x => ShowNewItem(x));

        for (int i = 0; playerSeenItems != null && i < playerSeenItems.objectsOwned.Count; i++)
        {
            if (playerSeenItems.objectsOwned[i].Substring(0, 1).Equals("0") && allCards.Items.Exists(x => x.Data.ID.Equals(playerSeenItems.objectsOwned[i])))
            {
                ShowNewShadow(allCards.Items.Find(x => x.Data.ID.Equals(playerSeenItems.objectsOwned[i])));
            }
        }
        LayoutRebuilder.ForceRebuildLayoutImmediate(ownedPanel);
        LayoutRebuilder.ForceRebuildLayoutImmediate(seenPanel);
    }
}
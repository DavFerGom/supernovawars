﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardTypesUsage : MonoBehaviour
{
    public SingleCardType myType;
    public SingleCardType typeToCompare;

    // Use this for initialization
    void Awake () {
        Debug.Log(myType);
        if (myType == typeToCompare)
        {
            Debug.Log("Both are same.");
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}